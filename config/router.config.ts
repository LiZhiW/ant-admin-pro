// import { IRoute } from 'umi';

interface RuntimeRouterConfig extends Omit<RuntimeRouter, 'component' | 'routes'> {
  /** 配置 location 和 path 匹配后用于渲染的 React 组件路径。可以是绝对路径，也可以是相对路径，如果是相对路径，会从 src/pages 开始找起 */
  component?: string;
  /** 配置子路由，通常在需要为多个路径增加 layout 组件时使用 */
  routes?: RuntimeRouterConfig[];
}

/**
 * 处理路由配置(转换成运行时配置)<br/>
 * 1.设置默认值<br/>
 * 2.自动生成国际化文本ID<br/>
 * 3.拼接path，简化原始配置<br/>
 * 4.TODO 自动生成的404页面<br/>
 * @param current 当前路由配置
 * @param parent  父级路由配置
 */
const routerToRuntimeConfig = (current: RouterConfig, parent?: RouterConfig): RuntimeRouterConfig => {
  const {
    path,
    exact,
    component,
    routes,
    wrappers,
    title,
    redirect,
    icon,
    pathVariable,
    querystring,
    name,
    nameLocale,
    pageTitle,
    pageTitleLocale,
    breadcrumbName,
    breadcrumbNameLocale,
    breadcrumbChildren,
    hideBreadcrumb,
    groupName,
    groupNameLocale,
    defaultOpen,
    hideMenu,
    hideChildrenMenu,
    enableLocale,
    authority,
    ...props
  } = current;
  current.name = name ?? 'NewPage';
  current.nameLocale = nameLocale ?? `${parent?.nameLocale ?? 'menu'}.${current.name}`;
  current.pageTitle = pageTitle ?? current.name;
  current.pageTitleLocale = pageTitleLocale ?? `${current.nameLocale}.title`;
  current.breadcrumbName = breadcrumbName ?? current.name;
  current.breadcrumbNameLocale = breadcrumbNameLocale ?? `${current.nameLocale}.breadcrumb`;
  if (groupName) {
    current.groupNameLocale = groupNameLocale ?? `${current.nameLocale}.group`;
  }
  const runtimeRouterConfig: RuntimeRouterConfig = {
    path: path,
    // exact: exact,
    // component: component,
    // routes: [],
    // wrappers: wrappers,
    // title: title,
    // redirect: redirect,
    icon: icon,
    pathVariable: pathVariable ?? {},
    querystring: querystring ?? {},
    name: current.name,
    nameLocale: current.nameLocale,
    pageTitle: current.pageTitle,
    pageTitleLocale: current.pageTitleLocale,
    breadcrumbName: current.breadcrumbName,
    breadcrumbNameLocale: current.breadcrumbNameLocale,
    breadcrumbChildren: [],
    hideBreadcrumb: hideBreadcrumb ?? false,
    groupName: groupName,
    groupNameLocale: current.groupNameLocale,
    hideMenu: hideMenu ?? false,
    hideChildrenMenu: hideChildrenMenu ?? false,
    authority: authority,
    ...props,
  };
  if (exact === false || exact === true) {
    runtimeRouterConfig.exact = exact;
  }
  if (component) {
    runtimeRouterConfig.component = component;
  }
  if (routes) {
    runtimeRouterConfig.routes = [];
    // 递归处理子路由
    routes.forEach((childRoute) => {
      runtimeRouterConfig.routes?.push(routerToRuntimeConfig(childRoute, current));
    });
  }
  if (wrappers) {
    runtimeRouterConfig.wrappers = wrappers;
  }
  if (title) {
    runtimeRouterConfig.title = title;
  }
  if (redirect) {
    runtimeRouterConfig.redirect = redirect;
  }
  if (breadcrumbChildren) {
    breadcrumbChildren.forEach((childBreadcrumb, index) => {
      const { redirectPath, pathVariable, querystring, breadcrumbName, breadcrumbNameLocale } = childBreadcrumb;
      childBreadcrumb.breadcrumbName = breadcrumbName ?? `ChildBreadcrumb-${index}`;
      childBreadcrumb.breadcrumbNameLocale = breadcrumbNameLocale ?? `${current.nameLocale}.breadcrumb.child.${childBreadcrumb.breadcrumbName}`;
      runtimeRouterConfig.breadcrumbChildren.push({
        breadcrumbName: childBreadcrumb.breadcrumbName,
        breadcrumbNameLocale: childBreadcrumb.breadcrumbNameLocale,
        redirectPath: redirectPath,
        pathVariable: pathVariable ?? {},
        querystring: querystring ?? {},
      });
    });
  }
  return runtimeRouterConfig;
};

/** 国际化文本 */
const localeArray: Array<string> = [];
/** 填充国际化文本 */
const fillLocaleArray = (route: RuntimeRouterConfig): void => {
  localeArray.push(`'${route.nameLocale}': '${route.name}',`);
  localeArray.push(`'${route.pageTitleLocale}': '${route.pageTitle}',`);
  localeArray.push(`'${route.breadcrumbNameLocale}': '${route.breadcrumbName}',`);
  route.breadcrumbChildren?.forEach((childBreadcrumb) => {
    localeArray.push(`'${childBreadcrumb.breadcrumbNameLocale}': '${childBreadcrumb.breadcrumbName}',`);
  });
  if (route.groupName) {
    localeArray.push(`'${route.groupNameLocale}': '${route.groupName}',`);
  }
  localeArray.push('');
  // 递归填充
  route.routes?.forEach((child) => fillLocaleArray(child));
};

/** 处理原始路由配置 */
const convertRoutesConfig = (routesConfig: RouterConfig[]): RuntimeRouterConfig[] => {
  const runtimeRoutersConfig: RuntimeRouterConfig[] = [];
  routesConfig.forEach((routeItem) => {
    const runtimeRouterConfig = routerToRuntimeConfig(routeItem);
    runtimeRoutersConfig.push(runtimeRouterConfig);
    fillLocaleArray(runtimeRouterConfig);
  });
  return runtimeRoutersConfig;
};

const routesConfig: RouterConfig[] = [
  {
    path: '/demo',
    name: '示例',
    component: '@/layouts/TestLayout',
    routes: [
      { path: '/demo/page', name: '页面1', icon: 'PictureOutlined', component: '@/pages/DemoPage' },
      { path: '/demo/page2', name: '页面2', icon: 'PictureOutlined', component: '@/pages/DemoPage' },
    ],
  },
  // NestSideLayout
  {
    path: '/nest-side-menu',
    name: 'demo',
    // component: '@/layouts/NestSideLayout',
    component: '@/layouts/TopSideLayout',
    routes: [
      {
        path: '/nest-side-menu/01',
        name: 'demo-01',
        icon: 'icon-anquanbaozhang',
        routes: [
          { path: '/nest-side-menu/01/01', name: 'demo-01-01', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/01/02', name: 'demo-01-02', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          {
            path: '/nest-side-menu/01/03',
            name: 'demo-01-03',
            icon: 'BarsOutlined',
            routes: [
              { path: '/nest-side-menu/01/03/01', name: '01-03-01', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
              { path: '/nest-side-menu/01/03/02', name: '01-03-02', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
              { path: '/nest-side-menu/01/03/03', name: '01-03-03', icon: 'BarsOutlined', groupName: 'group_1', component: '@/pages/Demo/PageWrapper_02' },
              { path: '/nest-side-menu/01/03/04', name: '01-03-04', icon: 'BarsOutlined', groupName: 'group_1', component: '@/pages/Demo/PageWrapper_01' },
              { path: '/nest-side-menu/01/03/05', name: '01-03-05', icon: 'BarsOutlined', groupName: 'group_2', component: '@/pages/Demo/PageWrapper_02' },
              { path: '/nest-side-menu/01/03/06', name: '01-03-06', icon: 'BarsOutlined', groupName: 'group_2', component: '@/pages/Demo/PageWrapper_01' },
            ],
          },
          { path: '/nest-side-menu/01/04', name: 'demo-01-04', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          {
            path: '/nest-side-menu/01/05',
            name: 'demo-01-05',
            icon: 'BarsOutlined',
            breadcrumbChildren: [
              { redirectPath: '/nest-side-menu/01/01', breadcrumbName: 'demo-01-01' },
              { redirectPath: '/nest-side-menu/01/02', breadcrumbName: 'demo-01-02' },
            ],
            component: '@/pages/Demo/PageWrapper_02',
          },
          { path: '/nest-side-menu/01/05/:var', name: 'demo-01-05-var', icon: 'BarsOutlined', hideMenu: true, component: '@/pages/Demo/PageWrapper_01' },
        ],
      },
      {
        path: '/nest-side-menu/02',
        name: 'demo-02',
        icon: 'BarsOutlined',
        routes: [
          { path: '/nest-side-menu/02/01', name: 'demo-02-01', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/02/02', name: 'demo-02-02', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/02/03', name: 'demo-02-03', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/02/04', name: 'demo-02-04', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/02/05', name: 'demo-02-05', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
        ],
      },
      {
        path: '/nest-side-menu/03',
        name: 'demo-03',
        icon: 'icon-anquanbaozhang',
        routes: [
          { path: '/nest-side-menu/03/01', name: 'demo-03-01', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/03/02', name: 'demo-03-02', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/03/03', name: 'demo-03-03', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/03/04', name: 'demo-03-04', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/03/05', name: 'demo-03-05', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
        ],
      },
      {
        path: '/nest-side-menu/04',
        name: 'demo-04',
        icon: 'BarsOutlined',
        routes: [
          { path: '/nest-side-menu/04/01', name: 'demo-04-01', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/04/02', name: 'demo-04-02', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/04/03', name: 'demo-04-03', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/04/04', name: 'demo-04-04', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/04/05', name: 'demo-04-05', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
        ],
      },
      {
        path: '/nest-side-menu/05',
        name: 'demo-05',
        icon: 'BarsOutlined',
        routes: [
          { path: '/nest-side-menu/05/01', name: 'demo-05-01', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/05/02', name: 'demo-05-02', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/05/03', name: 'demo-05-03', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
          { path: '/nest-side-menu/05/04', name: 'demo-05-04', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
          { path: '/nest-side-menu/05/05', name: 'demo-05-05', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
        ],
      },
      { path: '/nest-side-menu/06', name: 'demo-06', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_02' },
      { path: '/nest-side-menu/07', name: 'demo-07', icon: 'BarsOutlined', component: '@/pages/Demo/PageWrapper_01' },
    ],
  },
  // 自定义业务组件
  {
    path: '/component',
    name: '自定义组件',
    component: '@/layouts/NestSideLayout',
    // component: '@/layouts/TopSideLayout',
    routes: [
      // 通用组件
      {
        path: '/component/general',
        name: '通用组件',
        icon: 'AppstoreOutlined',
        routes: [
          { path: '/component/general/icon', name: 'AntdIcon(图标)', icon: 'PictureOutlined', component: '@/pages/Component/General/AntdIconDemo' },
          { path: '/component/general/toolbar', name: 'ToolBar(工具栏)', icon: 'ToolOutlined', component: '@/pages/Component/General/ToolBarDemo' },
        ],
      },
      // 导航组件
      {
        path: '/component/navigation',
        name: '导航组件',
        icon: 'MenuOutlined',
        routes: [{ path: '/component/navigation/001', name: '空白页', icon: 'PictureOutlined', component: '@/pages/Component/EmptyPage' }],
      },
      // 页面布局
      {
        path: '/component/layout',
        name: '页面布局',
        icon: 'LayoutOutlined',
        routes: [{ path: '/component/layout/001', name: '空白页', icon: 'PictureOutlined', component: '@/pages/Component/EmptyPage' }],
      },
      // 数据录入
      {
        path: '/component/data_entry',
        name: '数据录入',
        icon: 'FormOutlined',
        routes: [{ path: '/component/data_entry/001', name: '空白页', icon: 'PictureOutlined', component: '@/pages/Component/EmptyPage' }],
      },
      // 数据展示
      {
        path: '/component/data_display',
        name: '数据展示',
        icon: 'ProfileOutlined',
        routes: [
          {
            path: '/component/data_display/detail',
            name: '数据详情',
            icon: 'ContainerOutlined',
            routes: [
              { path: '/component/data_display/detail/detail_table', name: '详情表格', component: '@/pages/Component/Detail/DetailTableDemo' },
              { path: '/component/data_display/detail/detail_modal', name: '对话框详情表格', component: '@/pages/Component/Detail/DetailModalDemo' },
            ],
          },
          {
            path: '/component/data_display/table',
            name: 'Table表格',
            icon: 'TableOutlined',
            routes: [
              { path: '/component/data_display/table/data_table_base', name: '数据表格(基本)', component: '@/pages/Component/Table/DataTableBaseDemo' },
              { path: '/component/data_display/table/data_table_data', name: '数据表格(数据)', component: '@/pages/Component/Table/DataTableDataDemo' },
            ],
          },
        ],
      },
      // 反馈组件
      {
        path: '/component/feedback',
        name: '反馈组件',
        icon: 'SmileOutlined',
        routes: [{ path: '/component/feedback/001', name: '空白页', icon: 'PictureOutlined', component: '@/pages/Component/EmptyPage' }],
      },
      // 其他组件
      {
        path: '/component/other',
        name: '其他组件',
        icon: 'EllipsisOutlined',
        routes: [
          { path: '/component/other/antd_demo', name: 'AntD组件示例', icon: 'AntDesignOutlined', component: '@/pages/Component/Other/AntDesignDemo' },
          { path: '/component/other/001', name: '空白页', icon: 'PictureOutlined', component: '@/pages/Component/EmptyPage' },
        ],
      },
    ],
  },
];

const routes: RuntimeRouterConfig[] = convertRoutesConfig(routesConfig);
// console.log(`routes -> \n${JSON.stringify(routes, null, 2)}\n`);
// console.log(`localeText -> \n${localeArray.join('\n')}`);
export { routes };
