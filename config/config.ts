import { defineConfig, IConfig } from 'umi';
import { aliOssConf, enableCDN } from './oss.config';
import { webpackPlugin } from './webpack-plugin.config';
import { proxyConfig } from './proxy.config';
import { routes } from './router.config';

const { NODE_ENV, API_GLOBAL_PREFIX } = process.env;
// api请求Path全局前缀
const apiGlobalPrefix = API_GLOBAL_PREFIX ?? '';
// 是否是生产环境
const isProdEnv = NODE_ENV === 'production';
/**
 * umi配置
 */
const umiConfig: IConfig = {
  publicPath: enableCDN ? `${aliOssConf.cdnUrl}/${aliOssConf.appPath}/${aliOssConf.appVersion}/` : '/',
  // runtimePublicPath: true,
  hash: true,
  history: { type: 'hash' },
  define: {
    API_GLOBAL_PREFIX: apiGlobalPrefix,
    ENABLE_CDN: enableCDN,
    IS_PROD_ENV: isProdEnv,
  },
  routes: routes,
  devServer: { port: 8000 },
  // theme: {'@primary-color': primaryColor},
  proxy: proxyConfig,
  targets: { ie: 11 },
  ignoreMomentLocale: true,
  manifest: { basePath: '/' },
  nodeModulesTransform: {
    type: 'none',
    exclude: [],
  },
  forkTSChecker: {},
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  chainWebpack: webpackPlugin,

  // umi插件配置
  antd: {
    dark: false,
    compact: true,
  },
  // dva插件配置
  dva: {
    hmr: true,
  },
  // locale插件配置
  locale: {
    default: 'zh-CN',
    antd: true,
    title: true,
    baseNavigator: true,
  },
};

export default defineConfig(umiConfig);
