/**
 * 代理配置
 */
interface ProxyConfig {
  [matchPath: string]: {
    target: string;
    changeOrigin: boolean;
    pathRewrite: {
      [matchPath: string]: string;
    };
  };
}

/**
 * 开发环境代理配置
 */
const proxyConfig: ProxyConfig = {
  '/api/': {
    target: 'http://template.msvc.top/',
    changeOrigin: true,
    pathRewrite: { '^': '' },
  },
};

export { proxyConfig };
