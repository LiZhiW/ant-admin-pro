import WebpackChain from 'webpack-chain';
import AntdDayjsWebpackPlugin from 'antd-dayjs-webpack-plugin';
import { WebpackAliyunOss } from './plugins/webpack-aliyun-oss';
import { aliOssConf, enableCDN } from './oss.config';

const webpackPlugin = (config: WebpackChain) => {
  // 使用dayjs替换Moment.js
  config.plugin('antd-dayjs-webpack-plugin').use(AntdDayjsWebpackPlugin);
  // CDN支持(静态资源上传到阿里OSS)
  if (enableCDN) {
    config.plugin('webpack-aliyun-oss').use(WebpackAliyunOss, [
      {
        // test: true,
        timeout: 1000 * 60 * 10,
        from: ['./dist/**', '!./dist/*.html', '!./dist/**/*.map', '!./dist/iframe-page/codemirror/**/*.html', '!./dist/iframe-page/monaco-editor/dev/**'],
        dist: `${aliOssConf.appPath}/${aliOssConf.appVersion}/`,
        region: aliOssConf.region,
        accessKeyId: aliOssConf.accessKeyId,
        accessKeySecret: aliOssConf.accessKeySecret,
        bucket: aliOssConf.bucket,
        // setOssPath(filePath: string) {
        //   return filePath;
        // },
        setHeaders(filePath: string) {
          return {
            // 缓存时间
            'Cache-Control': 'max-age=31536000',
          };
        },
      },
    ]);
  }
};

export { webpackPlugin };
