import React, { CSSProperties } from 'react';
import { Space } from 'antd';
import { SpaceProps } from 'antd/es/space';
import lodash from 'lodash';
import classNames from 'classnames';
import { TypeEnum, variableTypeOf } from '@/common';
import styles from './ToolBar.less';

interface ToolBarProps {
  // ----------------------------------------------------------------------------------- 初始值

  // ----------------------------------------------------------------------------------- 核心配置
  /** ToolBar宽度 */
  width?: number | string;
  /** ToolBar高度 */
  height?: number | string;
  /** 是否显示边框 */
  border?: boolean;
  /** 工具栏标题 */
  title?: React.ReactNode;
  /** 工具栏标题自定义样式 */
  titleStyle?: CSSProperties;
  /** 工具栏标题自定义class样式 */
  titleClassName?: string;
  /** 左侧内容 */
  leftContent?: React.ReactNode[];
  /** 左侧SpaceProps */
  leftSpaceProps?: SpaceProps;
  /** 中间内容 */
  centerContent?: React.ReactNode[];
  /** 中间SpaceProps */
  centerSpaceProps?: SpaceProps;
  /** 右侧内容 */
  rightContent?: React.ReactNode[];
  /** 右侧SpaceProps */
  rightSpaceProps?: SpaceProps;
  // ----------------------------------------------------------------------------------- 扩展配置
  /** 自定义左侧区域渲染逻辑 */
  leftRender?: (props: Omit<ToolBarProps, 'leftRender'>, className: string, elementMap: Map<String, React.ReactNode>) => Map<String, React.ReactNode>;
  /** 自定义中间动态宽度区域渲染逻辑 */
  centreRender?: (props: Omit<ToolBarProps, 'centreRender'>, className: string, elementMap: Map<String, React.ReactNode>) => Map<String, React.ReactNode>;
  /** 自定义右侧区域渲染逻辑 */
  rightRender?: (props: Omit<ToolBarProps, 'rightRender'>, className: string, elementMap: Map<String, React.ReactNode>) => Map<String, React.ReactNode>;
  /** 当前组件自定义样式 */
  style?: CSSProperties;
  /** 当前组件自定义class样式 */
  className?: string;
  /** 左侧区域自定义样式 */
  leftStyle?: CSSProperties;
  /** 左侧区域自定义class样式 */
  leftClassName?: string;
  /** 中间区域自定义样式 */
  centerStyle?: CSSProperties;
  /** 中间区域自定义class样式 */
  centerClassName?: string;
  /** 右侧区域自定义样式 */
  rightStyle?: CSSProperties;
  /** 右侧区域自定义class样式 */
  rightClassName?: string;
}

interface ToolBarState {}

class ToolBar extends React.Component<ToolBarProps, ToolBarState> {
  static defaultProps: Readonly<Partial<ToolBarProps>> = {
    width: '100%',
    height: 40,
    border: true,
    leftSpaceProps: { size: 'small' },
    centerSpaceProps: { size: 'small' },
    rightSpaceProps: { size: 'small' },
  };

  constructor(props: ToolBarProps) {
    super(props);
    this.state = {};
  }

  protected getToolBarLeft() {
    const { title, titleStyle, titleClassName, leftContent, leftSpaceProps, leftStyle, leftClassName } = this.props;
    let elementMap: Map<String, React.ReactNode> = new Map<String, React.ReactNode>();
    // 设置 title
    elementMap.set(
      'title',
      variableTypeOf(title) === TypeEnum.string ? (
        <div key="title" className={classNames(styles.title, titleClassName)} style={titleStyle}>
          {title}
        </div>
      ) : (
        title
      ),
    );
    // 设置 leftContent
    if (leftContent) {
      lodash.forEach(leftContent, (item, key) => {
        elementMap.set(`left-${key}`, item);
      });
    }
    // 自定义内容
    const { leftRender, ...otherProps } = this.props;
    if (leftRender instanceof Function) {
      elementMap = leftRender(otherProps, styles.toolBarLeft, elementMap);
    }
    // 渲染
    return (
      <div key="left" className={classNames(styles.toolBarLeft, leftClassName)} style={leftStyle}>
        <Space {...leftSpaceProps}>{[...elementMap.values()]}</Space>
      </div>
    );
  }

  protected getToolBarCenter() {
    const { centerContent, centerStyle, centerSpaceProps, centerClassName } = this.props;
    let elementMap: Map<String, React.ReactNode> = new Map<String, React.ReactNode>();
    // 设置 centerContent
    if (centerContent) {
      lodash.forEach(centerContent, (item, key) => {
        elementMap.set(`center-${key}`, item);
      });
    }
    // 自定义内容
    const { centreRender, ...otherProps } = this.props;
    if (centreRender instanceof Function) {
      elementMap = centreRender(otherProps, styles.toolBarCenter, elementMap);
    }
    // 渲染
    return (
      <div key="center" className={classNames(styles.toolBarCenter, centerClassName)} style={centerStyle}>
        <Space {...centerSpaceProps}>{[...elementMap.values()]}</Space>
      </div>
    );
  }

  protected getToolBarRight() {
    const { rightContent, rightStyle, rightSpaceProps, rightClassName } = this.props;
    let elementMap: Map<String, React.ReactNode> = new Map<String, React.ReactNode>();
    // 设置 rightContent
    if (rightContent) {
      lodash.forEach(rightContent, (item, key) => {
        elementMap.set(`right-${key}`, item);
      });
    }
    // 自定义内容
    const { centreRender, ...otherProps } = this.props;
    if (centreRender instanceof Function) {
      elementMap = centreRender(otherProps, styles.toolBarRight, elementMap);
    }
    // 渲染
    return (
      <div key="right" className={classNames(styles.toolBarRight, rightClassName)} style={rightStyle}>
        <Space {...rightSpaceProps}>{[...elementMap.values()]}</Space>
      </div>
    );
  }

  public render() {
    const { width, height, border, style, className } = this.props;
    const defaultStyle: CSSProperties = {};
    if (width) defaultStyle.width = width;
    if (height) defaultStyle.height = height;
    if (border) defaultStyle.border = '1px solid #e8e8e8';
    return (
      <div className={classNames(styles.toolBar, className)} style={{ ...defaultStyle, ...style }}>
        {this.getToolBarLeft()}
        {this.getToolBarCenter()}
        {this.getToolBarRight()}
      </div>
    );
  }

  // -------------------------------------------------------------------------------------------------------------- 外部方法
}

export { ToolBarProps, ToolBarState, ToolBar };
