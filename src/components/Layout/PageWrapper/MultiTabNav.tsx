import React, { CSSProperties } from 'react';
import { Dropdown, Menu } from 'antd';
import { ArrowLeftOutlined, ArrowRightOutlined, CloseOutlined, CloseSquareOutlined, HomeOutlined, LeftOutlined, MoreOutlined, RightOutlined } from '@ant-design/icons';
import ResizeObserver from 'rc-resize-observer';
import lodash from 'lodash';
import classNames from 'classnames';
import { browserType } from '@/common';
import { getLeftNode, getRightNode } from '@/common/layout/flex-horizontal-layout';
import { AntdMenuClickParam } from '../layout-types';
import styles from './MultiTabNav.less';

/** “更多”按钮操作选项key */
type MoreButtonEventKey = 'closeLeft' | 'closeRight' | 'closeOther' | 'closeAll';

interface MultiTabNavProps {
  /** 默认是否存在滚动条 */
  defaultHasScroll?: boolean;
  /** 是否存在滚动条状态变化事件 */
  hasScrollOnChange?: (hasScroll: boolean) => void;
  /** 多页签容器scrollLeft值 */
  multiTabScrollLeft?: number;
  /** 多页签容器scrollLeft值变化事件 */
  multiTabScrollLeftOnChange?: (scrollLeft: number) => void;
  /** 页签数据 */
  tabsData: MultiTabItem[];
  /** 是否显示跳回首页按钮 */
  showHomeButton?: boolean;
  /** 是否显示更多按钮 */
  showMoreButton?: boolean;
  /** 点击跳回首页按钮事件 */
  onClickHomeButton?: () => void;
  /** 点击更多按钮项事件 */
  onClickMoreButton?: (param: AntdMenuClickParam, eventKey: MoreButtonEventKey) => void;
  /** 点击RouterTabItem上的关闭按钮事件 */
  onCloseTab?: (multiTab: MultiTabItem) => void;
  /** 单击RouterTabItem上的标题事件 */
  onClickTab?: (multiTab: MultiTabItem) => void;
  // ----------------------------------------------------------------------------------- 自定义渲染逻辑
  /** class样式 */
  className?: string;
  /** 样式 */
  style?: CSSProperties;
  /** 左侧区域class样式 */
  leftClassName?: string;
  /** 左侧区域样式 */
  leftStyle?: CSSProperties;
  /** 中间动态宽度区域class样式 */
  centreClassName?: string;
  /** 中间动态宽度区域样式 */
  centreStyle?: CSSProperties;
  /** 左侧区域class样式 */
  rightClassName?: string;
  /** 左侧区域样式 */
  rightStyle?: CSSProperties;
  /** 多页签Tab class样式 */
  tabClassName?: string;
  /** 多页签Tab样式 */
  tabStyle?: CSSProperties;
  /** 多页签active Tab class样式 */
  tabActiveClassName?: string;
  /** Tab标题class样式 */
  tabTitleClassName?: string;
  /** Tab标题样式 */
  tabTitleStyle?: CSSProperties;
  /** Tab关闭按钮class样式 */
  tabCloseClassName?: string;
  /** Tab关闭按钮样式 */
  tabCloseStyle?: CSSProperties;
  /** 自定义左侧区域渲染逻辑 */
  leftRender?: (props: Omit<MultiTabNavProps, 'leftRender'>, className: string, elementMap: Map<String, React.ReactNode>) => React.ReactNode;
  /** 自定义右侧区域渲染逻辑 */
  rightRender?: (props: Omit<MultiTabNavProps, 'rightRender'>, className: string, elementMap: Map<String, React.ReactNode>) => React.ReactNode;
  /** 自定义多页签渲染 */
  tabRender?: (props: Omit<MultiTabNavProps, 'tabRender'>, tabItem: MultiTabItem, elementMap: Map<String, React.ReactNode>) => Map<String, React.ReactNode>;
  /** 自定义当前选中页签渲染 */
  activeTabRender?: (props: Omit<MultiTabNavProps, 'activeTabRender'>, tabItem: MultiTabItem, elementMap: Map<String, React.ReactNode>) => Map<String, React.ReactNode>;
}

interface MultiTabNavState {
  /** 是否存在滚动条 */
  hasScroll: boolean;
}

/**
 * 多标签页导航
 */
class MultiTabNav extends React.Component<MultiTabNavProps, MultiTabNavState> {
  state = {
    hasScroll: false,
  };

  /** 多页签Tab页容器Ref */
  multiTabRef: HTMLDivElement | null = null;

  /** 当前选中的页签Tab页Ref */
  currentTabRef: HTMLDivElement | null = null;

  /** 将要卸载组件 */
  willUnmount: boolean = false;

  constructor(props: MultiTabNavProps) {
    super(props);
    this.state = {
      hasScroll: props.defaultHasScroll ?? false,
    };
    // console.log('MultiTabNav -> constructor');
  }

  componentDidMount() {
    const { multiTabScrollLeft } = this.props;
    // console.log('MultiTabNav -> componentDidMount', multiTabScrollLeft);
    if (this.multiTabRef && multiTabScrollLeft && this.multiTabRef.scrollTo) {
      this.multiTabRef.scrollTo(multiTabScrollLeft, 0);
    }
  }

  componentWillUnmount() {
    // console.log('MultiTabNav -> componentWillUnmount');
    this.willUnmount = true;
  }

  componentDidUpdate(prevProps: Readonly<MultiTabNavProps>, prevState: Readonly<MultiTabNavState>, snapshot: any) {
    // console.log('MultiTabNav -> componentDidUpdate', prevProps);
    if (this.willUnmount) return;
    const { hasScroll } = this.state;
    const { hasScrollOnChange } = this.props;
    // multiTab-可视区域宽度
    let multiTabClientWidth: number | undefined;
    // multiTab-滚动区域宽度
    let multiTabScrollWidth: number | undefined;
    if (this.multiTabRef) {
      multiTabClientWidth = this.multiTabRef.clientWidth;
      multiTabScrollWidth = this.multiTabRef.scrollWidth;
    }
    // 当前页签滚动到可视区域
    if (this.currentTabRef && this.currentTabRef.scrollIntoView) {
      // 动态计算 inline 值 "start", "end", "nearest"
      this.currentTabRef.scrollIntoView({ block: 'nearest', behavior: 'smooth', inline: 'nearest' });
    }
    // 计算更新多页签是否右滚动条
    if (multiTabClientWidth && multiTabScrollWidth) {
      let newHasScroll: boolean | null = null;
      if (hasScroll && multiTabClientWidth >= multiTabScrollWidth) {
        newHasScroll = false;
      } else if (!hasScroll && multiTabClientWidth < multiTabScrollWidth) {
        newHasScroll = true;
      }
      // console.log("newHasScroll -> ", newHasScroll);
      if (newHasScroll === null) {
        return;
      }
      if (hasScrollOnChange instanceof Function) {
        this.state.hasScroll = newHasScroll;
        hasScrollOnChange(newHasScroll);
      } else {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState({ hasScroll: newHasScroll! });
      }
    }
  }

  // 中间动态宽度区域
  protected getCentre() {
    const { tabsData, centreClassName, centreStyle = {}, multiTabScrollLeftOnChange } = this.props;
    const multiTabNodeArray: React.ReactNode[] = [];
    tabsData.forEach((tab) => multiTabNodeArray.push(this.getRouterTabItem(tab)));
    return (
      <ResizeObserver
        onResize={lodash.debounce(() => {
          if (this.willUnmount) return;
          this.forceUpdate();
        }, 300)}
      >
        <div
          ref={(ref) => {
            this.multiTabRef = ref;
          }}
          className={classNames(styles.multiTabCenter, styles.multiTabContent, centreClassName)}
          style={centreStyle}
          onWheel={(e) => {
            if (!this.multiTabRef || e?.deltaY === 0) return false;
            let delta: number = e.deltaY;
            if (browserType.isFirefox) {
              delta *= 15;
            } else if (browserType.isChrome) {
              delta /= 3;
            }
            this.getMultiTabScrollLeft(delta);
            return false;
          }}
          onScroll={lodash.debounce(() => {
            // console.log('MultiTabNav getCentre onScroll -> ', this.multiTabRef?.scrollLeft);
            if (multiTabScrollLeftOnChange instanceof Function && this.multiTabRef) multiTabScrollLeftOnChange(this.multiTabRef.scrollLeft);
          }, 100)}
          // onScrollCapture={(event) => {}}
        >
          {multiTabNodeArray}
        </div>
      </ResizeObserver>
    );
  }

  // 跳回首页按钮
  protected getHomeButton(): React.ReactNode {
    const { onClickHomeButton } = this.props;
    return (
      <div
        key="homeButton"
        className={classNames(styles.multiTabButton)}
        onClick={() => {
          if (onClickHomeButton instanceof Function) {
            onClickHomeButton();
          }
        }}
      >
        <HomeOutlined />
      </div>
    );
  }

  // 左侧滚动条按钮
  protected getLeftScroll(): React.ReactNode {
    return (
      <div key="leftScroll" className={classNames(styles.multiTabButton)} onClick={() => this.getMultiTabScrollLeft(-80)}>
        <LeftOutlined />
      </div>
    );
  }

  // 右侧滚动条按钮
  protected getRightScroll(): React.ReactNode {
    return (
      <div key="rightScroll" className={classNames(styles.multiTabButton)} onClick={() => this.getMultiTabScrollLeft(80)}>
        <RightOutlined />
      </div>
    );
  }

  // 更多按钮
  protected getMoreButton(): React.ReactNode {
    const { onClickMoreButton } = this.props;
    return (
      <Dropdown
        key="moreButton"
        trigger={['click']}
        placement="bottomLeft"
        overlay={
          <Menu
            onClick={(event) => {
              if (onClickMoreButton instanceof Function) onClickMoreButton(event, event.key as MoreButtonEventKey);
            }}
          >
            <Menu.Item key="closeLeft">
              <ArrowLeftOutlined />
              关闭左侧
            </Menu.Item>
            <Menu.Item key="closeRight">
              <ArrowRightOutlined />
              关闭右侧
            </Menu.Item>
            <Menu.Item key="closeOther">
              <CloseOutlined />
              关闭其它
            </Menu.Item>
            <Menu.Item key="closeAll">
              <CloseSquareOutlined />
              全部关闭
            </Menu.Item>
          </Menu>
        }
      >
        <div className={classNames(styles.multiTabButton)}>
          <MoreOutlined />
        </div>
      </Dropdown>
    );
  }

  // 获取单个标签页
  protected getRouterTabItem(tabItem: MultiTabItem): React.ReactNode {
    const {
      onClickTab,
      onCloseTab,
      tabClassName,
      tabStyle,
      tabActiveClassName = '',
      tabTitleClassName,
      tabTitleStyle,
      tabCloseClassName,
      tabCloseStyle,
      tabRender,
      activeTabRender,
    } = this.props;
    let elementMap: Map<String, React.ReactNode> = new Map<String, React.ReactNode>();
    // Tab图标
    if (tabItem.isHomePage) {
      elementMap.set(
        'tabIcon',
        <div key="tabIcon" className={classNames(styles.pageHeaderTabIcon)}>
          <HomeOutlined />
        </div>,
      );
    }
    // Tab标题
    const name = tabItem.menuItem.routerConfig.breadcrumbName ?? tabItem.menuItem.routerConfig.name ?? '';
    elementMap.set(
      'tabTitle',
      <div key="tabTitle" className={classNames(styles.pageHeaderTabTitle, tabTitleClassName)} style={tabTitleStyle}>
        {name}
      </div>,
    );
    // 支持自定义渲染逻辑
    if (tabItem.active && activeTabRender instanceof Function) {
      const { activeTabRender: _, ...otherProps } = this.props;
      elementMap = activeTabRender(otherProps, tabItem, elementMap);
    } else if (tabRender instanceof Function) {
      const { tabRender: _, ...otherProps } = this.props;
      elementMap = tabRender(otherProps, tabItem, elementMap);
    }
    return (
      <div
        ref={(ref) => {
          if (tabItem.active) this.currentTabRef = ref;
        }}
        key={tabItem.multiTabKey ?? tabItem.menuItem.menuKey}
        className={classNames(styles.pageHeaderTab, { [styles.pageHeaderTabActive]: tabItem.active }, tabClassName, { [tabActiveClassName]: tabItem.active })}
        style={tabStyle}
        onClick={() => {
          if (onClickTab instanceof Function) onClickTab(tabItem);
        }}
      >
        {[...elementMap.values()]}
        {tabItem.showClose && (
          <CloseOutlined
            className={classNames(styles.pageHeaderTabClose, tabCloseClassName)}
            style={tabCloseStyle}
            onClick={(e) => {
              e.stopPropagation();
              if (onCloseTab instanceof Function) onCloseTab(tabItem);
            }}
          />
        )}
      </div>
    );
  }

  /**
   * 页面头部滚动 - MultiTab (左右滚动)
   */
  public getMultiTabScrollLeft(delta: number): void {
    if (!this.multiTabRef) return;
    // 是否要控制 delta 值在一个范围之内，如：30 ~ 180
    this.multiTabRef.scrollLeft += delta;
    // console.log('右侧滚动条按钮 -> ', this.multiTabRef.scrollLeft, delta);
  }

  public render() {
    // console.log("MultiTabNav render");
    const { className, style } = this.props;
    return (
      <div className={classNames(className, styles.multiTab)} style={style}>
        {/* 左侧区域 */}
        {getLeftNode<MultiTabNavProps>(this.props, styles.multiTabLeft, (elementMap) => {
          const { showHomeButton = true } = this.props;
          const { hasScroll } = this.state;
          // 是否显示跳回首页按钮
          if (showHomeButton) {
            elementMap.set('homeButton', this.getHomeButton());
          }
          // 左侧滚动条按钮
          if (hasScroll) {
            elementMap.set('leftScroll', this.getLeftScroll());
          }
        })}
        {/* 中间动态宽度区域 */}
        {this.getCentre()}
        {/* 右侧区域 */}
        {getRightNode<MultiTabNavProps>(this.props, styles.multiTabRight, (elementMap) => {
          const { showMoreButton = true } = this.props;
          const { hasScroll } = this.state;
          // 左侧滚动条按钮
          if (hasScroll) {
            elementMap.set('rightScroll', this.getRightScroll());
          }
          // 是否显示跳回首页按钮
          if (showMoreButton) {
            elementMap.set('moreButton', this.getMoreButton());
          }
        })}
      </div>
    );
  }
}

export { MoreButtonEventKey, MultiTabNavProps, MultiTabNavState, MultiTabNav };
