import React, { CSSProperties } from 'react';
import { PageHeader } from 'antd';
import classNames from 'classnames';
import SimpleBarReact from 'simplebar-react';
import 'simplebar/src/simplebar.css';
import { TypeEnum, umiHistory, variableTypeOf } from '@/common';
import { AntdBreadcrumbProps, AntdMenuClickParam, AntdPageHeaderProps, AntdTagType } from '../layout-types';
import { PageContext, PageContextProps, PageHeaderModel } from '../PageContext';
import { MoreButtonEventKey, MultiTabNav, MultiTabNavProps } from './MultiTabNav';
import styles from './PageWrapper.less';

/** 页面参数 */
interface PageWrapperProps {
  /** pageHeaderModel=“MultiTab”时，是否启用 PageHeader 组件 */
  enablePageHeader?: boolean;
  // ----------------------------------------------------------------------------------- MultiTab 配置
  /** 点击跳回首页按钮事件 */
  onClickHomeButton?: () => void;
  /** 点击更多按钮项事件 */
  onClickMoreButton?: (param: AntdMenuClickParam, eventKey: MoreButtonEventKey) => void;
  /** 点击RouterTabItem上的关闭按钮事件 */
  onCloseTab?: (multiTab: MultiTabItem) => void;
  /** 单击RouterTabItem上的标题事件 */
  onClickTab?: (multiTab: MultiTabItem) => void;
  /** 左侧区域class样式 */
  leftClassName?: string;
  /** 左侧区域样式 */
  leftStyle?: CSSProperties;
  /** 中间动态宽度区域class样式 */
  centreClassName?: string;
  /** 中间动态宽度区域样式 */
  centreStyle?: CSSProperties;
  /** 左侧区域class样式 */
  rightClassName?: string;
  /** 左侧区域样式 */
  rightStyle?: CSSProperties;
  /** 多页签Tab class样式 */
  tabClassName?: string;
  /** 多页签Tab样式 */
  tabStyle?: CSSProperties;
  /** 多页签active Tab class样式 */
  tabActiveClassName?: string;
  /** Tab标题class样式 */
  tabTitleClassName?: string;
  /** Tab标题样式 */
  tabTitleStyle?: CSSProperties;
  /** Tab关闭按钮class样式 */
  tabCloseClassName?: string;
  /** Tab关闭按钮样式 */
  tabCloseStyle?: CSSProperties;
  /** 自定义当前选中页签渲染 */
  activeTabRender?: (props: Omit<MultiTabNavProps, 'activeTabRender'>, tabItem: MultiTabItem, elementMap: Map<String, React.ReactNode>) => Map<String, React.ReactNode>;
  // ----------------------------------------------------------------------------------- AntPageHeader 配置
  /** 返回上一页 */
  onBack?: boolean | (() => void) | string | UmiLocation;
  /** 页面标题 */
  pageHeaderTitle?: React.ReactNode;
  /** 页面描述 */
  pageHeaderSubTitle?: React.ReactNode;
  /** title 旁的 tag 列表 */
  pageHeaderTags?: React.ReactElement<AntdTagType> | React.ReactElement<AntdTagType>[];
  /** 面包屑配置 */
  pageHeaderBreadcrumb?: AntdBreadcrumbProps;
  /** PageHeader内容 */
  pageHeaderContent?: React.ReactNode;
  /** 操作区，位于 title 行的行尾 */
  pageHeaderExtra?: React.ReactNode;
  /** PageHeader 的页脚，一般用于渲染 TabBar */
  pageHeaderFooter?: React.ReactNode;
  /** PageHeader扩展属性 */
  pageHeaderProps?: AntdPageHeaderProps;
  /** Ant PageHeader组件class样式 */
  pageHeaderClassName?: string;
  /** Ant PageHeader组件样式 */
  pageHeaderStyle?: CSSProperties;
  // ----------------------------------------------------------------------------------- PageContent 配置
  /** 是否美化滚动条 */
  beautifyScrollbar?: boolean;
  /** 是否自动隐藏页面滚动条(beautifyScrollbar = true有用) */
  autoHideScrollbar?: boolean;
  /** 自定义美化滚动条class样式 */
  scrollbarClassName?: string;
  // ----------------------------------------------------------------------------------- 扩展配置
  /** 页面class样式 */
  contentClassName?: string;
  /** 页面样式 */
  contentStyle?: CSSProperties;
}

interface PageWrapperState {}

/**
 * 页面包装器
 */
class PageWrapper extends React.Component<PageWrapperProps, PageWrapperState> {
  // 定义使用 PageContext
  static contextType = PageContext;

  // 获取PageContext值
  protected getContext(): PageContextProps {
    return this.context as PageContextProps;
  }

  // 页面头部
  protected getPageHeader() {
    const { pageHeaderModel = PageHeaderModel.AntPageHeader, forceHideMultiTab, forceHideAntPageHeader } = this.getContext();
    if (pageHeaderModel === PageHeaderModel.MultiTab) {
      return this.getMultiTab(forceHideMultiTab);
    }
    return this.getAntPageHeader(true, forceHideAntPageHeader);
  }

  // 页面内容
  protected getPageContent() {
    const { beautifyScrollbar, autoHideScrollbar, scrollbarClassName, contentClassName, contentStyle = {}, children } = this.props;
    const {
      pageHeaderModel = PageHeaderModel.AntPageHeader,
      beautifyScrollbar: beautifyScrollbarCtx = true,
      autoHideScrollbar: autoHideScrollbarCtx = false,
      scrollbarClassName: scrollbarClassNameCtx,
      contentClassName: contentClassNameCtx,
      contentStyle: contentStyleCtx = {},
    } = this.getContext();
    const usePageHeader = this.getEnablePageHeader();
    // 是否要美化滚动条
    let scrollbar: boolean = true;
    if (beautifyScrollbar !== undefined) {
      scrollbar = beautifyScrollbar;
    } else if (beautifyScrollbarCtx !== undefined) {
      scrollbar = beautifyScrollbarCtx;
    }
    if (!scrollbar) {
      return (
        <div
          className={classNames(
            styles.pageContent,
            { [styles.multiTabContent]: pageHeaderModel === PageHeaderModel.MultiTab && !usePageHeader },
            contentClassNameCtx,
            contentClassName,
          )}
          style={{ ...contentStyleCtx, ...contentStyle }}
        >
          {children}
        </div>
      );
    }
    // 滚动条是否自动隐藏
    let autoHide: boolean = false;
    if (autoHideScrollbar !== undefined) {
      autoHide = autoHideScrollbar;
    } else if (autoHideScrollbarCtx !== undefined) {
      autoHide = autoHideScrollbarCtx;
    }
    return (
      <div className={classNames(styles.simpleBarContent, { [styles.multiTabContent]: pageHeaderModel === PageHeaderModel.MultiTab && !usePageHeader })}>
        <SimpleBarReact className={classNames(styles.simpleBar, scrollbarClassNameCtx, scrollbarClassName)} style={{ height: '100%', width: '100%' }} autoHide={autoHide}>
          <div className={classNames(styles.content, contentClassNameCtx, contentClassName)} style={{ ...contentStyleCtx, ...contentStyle }}>
            {children}
          </div>
        </SimpleBarReact>
      </div>
    );
  }

  // 页面头部 - AntPageHeader
  protected getAntPageHeader(renderBreadcrumb: boolean = true, forceHideAntPageHeader: boolean = false) {
    const {
      onBack,
      pageHeaderTitle,
      pageHeaderSubTitle,
      pageHeaderTags,
      pageHeaderBreadcrumb,
      pageHeaderContent,
      pageHeaderExtra,
      pageHeaderFooter,
      pageHeaderProps,
      pageHeaderClassName,
      pageHeaderStyle,
    } = this.props;
    const {
      onBack: onBackCtx,
      pageHeaderTitle: pageHeaderTitleCtx,
      pageHeaderSubTitle: pageHeaderSubTitleCtx,
      pageHeaderTags: pageHeaderTagsCtx,
      pageHeaderBreadcrumb: pageHeaderBreadcrumbCtx,
      pageHeaderContent: pageHeaderContentCtx,
      pageHeaderExtra: pageHeaderExtraCtx,
      pageHeaderFooter: pageHeaderFooterCtx,
      pageHeaderProps: pageHeaderPropsCtx,
      pageHeaderClassName: pageHeaderClassNameCtx,
      pageHeaderStyle: pageHeaderStyleCtx,
    } = this.getContext();
    const onBackDefault: boolean = !!(pageHeaderTitle || pageHeaderSubTitle || pageHeaderTags || pageHeaderTitleCtx || pageHeaderSubTitleCtx || pageHeaderTagsCtx);
    // 返回按钮的点击事件
    let onBackProp: Partial<AntdPageHeaderProps> = this.getOnBackProp(onBack ?? (onBack === undefined && onBackCtx === undefined && onBackDefault));
    if (!onBackProp.onBack) onBackProp = this.getOnBackProp(onBackCtx ?? (onBack === undefined && onBackCtx === undefined && onBackDefault));
    // 面包屑配置
    const breadcrumbProp: Partial<AntdPageHeaderProps> = {};
    if (renderBreadcrumb && (pageHeaderBreadcrumb || pageHeaderBreadcrumbCtx)) {
      breadcrumbProp.breadcrumb = { ...pageHeaderBreadcrumbCtx, ...pageHeaderBreadcrumb };
    }
    // console.log("pageHeaderBreadcrumbCtx -> ", pageHeaderBreadcrumbCtx, pageHeaderBreadcrumb, { ...pageHeaderBreadcrumbCtx, ...pageHeaderBreadcrumb });
    // 标题文字
    const titleProp: Partial<AntdPageHeaderProps> = {};
    if (pageHeaderTitle || pageHeaderTitleCtx) {
      titleProp.title = pageHeaderTitle ?? pageHeaderTitleCtx;
    }
    // 二级标题文字
    const subTitleProp: Partial<AntdPageHeaderProps> = {};
    if (pageHeaderSubTitle || pageHeaderSubTitleCtx) {
      subTitleProp.subTitle = pageHeaderSubTitle ?? pageHeaderSubTitleCtx;
    }
    // title 旁的 tag 列表
    const tagsProp: Partial<AntdPageHeaderProps> = {};
    if (pageHeaderTags || pageHeaderTagsCtx) {
      tagsProp.tags = pageHeaderTags ?? pageHeaderTagsCtx;
    }
    // 操作区，位于 title 行的行尾
    const extraProp: Partial<AntdPageHeaderProps> = {};
    if (pageHeaderExtra || pageHeaderExtraCtx) {
      extraProp.extra = pageHeaderExtra ?? pageHeaderExtraCtx;
    }
    // PageHeader 的页脚，一般用于渲染 TabBar
    const footerProp: Partial<AntdPageHeaderProps> = {};
    if (pageHeaderFooter || pageHeaderFooterCtx) {
      footerProp.footer = pageHeaderFooter ?? pageHeaderFooterCtx;
    }
    // 合并PageHeaderProps属性
    const mergePageHeaderProps: any = {
      ...onBackProp,
      ...breadcrumbProp,
      ...titleProp,
      ...subTitleProp,
      ...tagsProp,
      ...extraProp,
      ...footerProp,
      ...pageHeaderPropsCtx,
      ...pageHeaderProps,
    };
    if (forceHideAntPageHeader) {
      mergePageHeaderProps.breadcrumb = undefined;
      if (
        !mergePageHeaderProps.title &&
        !mergePageHeaderProps.subTitle &&
        !mergePageHeaderProps.tags &&
        !mergePageHeaderProps.footer &&
        !mergePageHeaderProps.extra &&
        !mergePageHeaderProps.avatar &&
        !mergePageHeaderProps.onBack
      ) {
        return <div style={{ height: 1, backgroundColor: '#e3e3e3' }} />;
      }
    }
    return (
      <PageHeader
        className={classNames(styles.pageHeader, pageHeaderClassName, pageHeaderClassNameCtx)}
        style={{ ...pageHeaderStyleCtx, ...pageHeaderStyle }}
        {...mergePageHeaderProps}
      >
        {pageHeaderContent ?? pageHeaderContentCtx}
      </PageHeader>
    );
  }

  // 页面头部 - MultiTab
  protected getMultiTab(forceHideMultiTab: boolean = false) {
    const {
      onClickHomeButton,
      onClickMoreButton,
      onCloseTab,
      onClickTab,
      leftClassName,
      leftStyle,
      centreClassName,
      centreStyle,
      rightClassName,
      rightStyle,
      tabClassName,
      tabStyle,
      tabActiveClassName,
      tabTitleClassName,
      tabTitleStyle,
      tabCloseClassName,
      tabCloseStyle,
      activeTabRender,
    } = this.props;
    const {
      defaultHasScroll: defaultHasScrollCtx,
      hasScrollOnChange: hasScrollOnChangeCtx,
      multiTabScrollLeft: multiTabScrollLeftCtx,
      multiTabScrollLeftOnChange: multiTabScrollLeftOnChangeCtx,
      tabsData: tabsDataCtx,
      showHomeButton: showHomeButtonCtx,
      showMoreButton: showMoreButtonCtx,
      onClickHomeButton: onClickHomeButtonCtx,
      onClickMoreButton: onClickMoreButtonCtx,
      onCloseTab: onCloseTabCtx,
      onClickTab: onClickTabCtx,
      multiTabNavClassName: multiTabNavClassNameCtx,
      multiTabNavStyle: multiTabNavStyleCtx,
      leftClassName: leftClassNameCtx,
      leftStyle: leftStyleCtx,
      centreClassName: centreClassNameCtx,
      centreStyle: centreStyleCtx,
      rightClassName: rightClassNameCtx,
      rightStyle: rightStyleCtx,
      tabClassName: tabClassNameCtx,
      tabStyle: tabStyleCtx,
      tabActiveClassName: tabActiveClassNameCtx,
      tabTitleClassName: tabTitleClassNameCtx,
      tabTitleStyle: tabTitleStyleCtx,
      tabCloseClassName: tabCloseClassNameCtx,
      tabCloseStyle: tabCloseStyleCtx,
      leftRender: leftRenderCtx,
      rightRender: rightRenderCtx,
      tabRender: tabRenderCtx,
      activeTabRender: activeTabRenderCtx,
      forceHideAntPageHeader: forceHideAntPageHeaderCtx,
    } = this.getContext();
    let activeTabRenderImpl: MultiTabNavProps['activeTabRender'];
    if (activeTabRender instanceof Function) {
      activeTabRenderImpl = activeTabRender;
    } else if (activeTabRenderCtx instanceof Function) {
      activeTabRenderImpl = activeTabRenderCtx;
    }
    const usePageHeader = this.getEnablePageHeader();
    // console.log("tabsDataCtx -> ", tabsDataCtx);
    return (
      <>
        {!forceHideMultiTab && (
          <MultiTabNav
            className={classNames(styles.pageMultiTabNav, multiTabNavClassNameCtx)}
            style={multiTabNavStyleCtx}
            defaultHasScroll={defaultHasScrollCtx}
            hasScrollOnChange={hasScrollOnChangeCtx}
            multiTabScrollLeft={multiTabScrollLeftCtx}
            multiTabScrollLeftOnChange={multiTabScrollLeftOnChangeCtx}
            tabsData={tabsDataCtx}
            showHomeButton={showHomeButtonCtx}
            showMoreButton={showMoreButtonCtx}
            onClickHomeButton={() => {
              // console.log("PageWrapper - onClickHomeButton");
              if (onClickHomeButton instanceof Function) {
                onClickHomeButton();
              }
              if (onClickHomeButtonCtx instanceof Function) {
                onClickHomeButtonCtx();
              }
            }}
            onClickMoreButton={(param, eventKey) => {
              // console.log("PageWrapper - onClickMoreButton", param, eventKey);
              if (onClickMoreButton instanceof Function) {
                onClickMoreButton(param, eventKey);
              }
              if (onClickMoreButtonCtx instanceof Function) {
                onClickMoreButtonCtx(param, eventKey);
              }
            }}
            onCloseTab={(multiTab) => {
              // console.log("PageWrapper - onCloseTab", multiTab);
              if (onCloseTab instanceof Function) {
                onCloseTab(multiTab);
              }
              if (onCloseTabCtx instanceof Function) {
                onCloseTabCtx(multiTab);
              }
            }}
            onClickTab={(multiTab) => {
              // console.log("PageWrapper - onClickTab", multiTab);
              if (onClickTab instanceof Function) {
                onClickTab(multiTab);
              }
              if (onClickTabCtx instanceof Function) {
                onClickTabCtx(multiTab);
              }
            }}
            leftClassName={classNames(leftClassNameCtx, leftClassName)}
            leftStyle={{ ...leftStyleCtx, ...leftStyle }}
            centreClassName={classNames(centreClassNameCtx, centreClassName)}
            centreStyle={{ ...centreStyleCtx, ...centreStyle }}
            rightClassName={classNames(rightClassNameCtx, rightClassName)}
            rightStyle={{ ...rightStyleCtx, ...rightStyle }}
            tabClassName={classNames(tabClassNameCtx, tabClassName)}
            tabStyle={{ ...tabStyleCtx, ...tabStyle }}
            tabActiveClassName={classNames(tabActiveClassNameCtx, tabActiveClassName)}
            tabTitleClassName={classNames(tabTitleClassNameCtx, tabTitleClassName)}
            tabTitleStyle={{ ...tabTitleStyleCtx, ...tabTitleStyle }}
            tabCloseClassName={classNames(tabCloseClassNameCtx, tabCloseClassName)}
            tabCloseStyle={{ ...tabCloseStyleCtx, ...tabCloseStyle }}
            leftRender={leftRenderCtx}
            rightRender={rightRenderCtx}
            tabRender={tabRenderCtx}
            activeTabRender={activeTabRenderImpl}
          />
        )}
        {usePageHeader && this.getAntPageHeader(false, forceHideAntPageHeaderCtx)}
      </>
    );
  }

  // pageHeaderModel="MultiTab"时，是否启用 PageHeader 组件
  protected getEnablePageHeader(): boolean {
    const { enablePageHeader } = this.props;
    const { enablePageHeader: enablePageHeaderCtx } = this.getContext();
    let usePageHeader: boolean = false;
    if (enablePageHeader !== undefined) {
      usePageHeader = enablePageHeader;
    } else if (enablePageHeaderCtx !== undefined) {
      usePageHeader = enablePageHeaderCtx;
    }
    return usePageHeader;
  }

  // 返回按钮的点击事件属性
  protected getOnBackProp(onBack?: boolean | (() => void) | string | UmiLocation): Partial<AntdPageHeaderProps> {
    const onBackProp: Partial<AntdPageHeaderProps> = {};
    if (onBack !== undefined && onBack !== false) {
      if (onBack === true) {
        onBackProp.onBack = () => umiHistory.goBack();
      } else if (onBack instanceof Function) {
        onBackProp.onBack = onBack;
      } else if (variableTypeOf(onBack) === TypeEnum.string) {
        onBackProp.onBack = () => umiHistory.push({ pathname: onBack as string });
      } else if (variableTypeOf(onBack) === TypeEnum.object) {
        onBackProp.onBack = () => umiHistory.push(onBack as UmiLocation);
      }
    }
    return onBackProp;
  }

  public render() {
    // const pageContext: PageContextType = this.context as PageContextType;
    // console.log("this.context --> ", pageContext);
    // console.log("this.props --> ", this.props.pageHeaderClassName);
    return (
      <>
        {/* 页面头部 */}
        {this.getPageHeader()}
        {/* 页面内容 */}
        {this.getPageContent()}
      </>
    );
  }
}

export { PageWrapperProps, PageWrapperState, PageWrapper };
