import memoizeOne from 'memoize-one';
import isEqual from 'lodash.isequal';
import lodash from 'lodash';
import stableStringify from 'fast-json-stable-stringify';
import { FormatMessage } from '@/components/Layout/layout-types';
import { isUrl, pathMatchMenu, pathToList } from './layouts-utils';

interface GlobalConfigContext {
  /** 菜单配置 */
  menuSettings: RouterMenuSettings;
  /** 国际化实现函数 */
  formatMessage?: FormatMessage;
}

/**
 * 根据路由项配置生成唯一key字符串(menuKey)
 */
const getMenuKey = (routerConfig: RuntimeRouter): string => {
  const { path, exact, redirect, pathVariable, querystring, name } = routerConfig;
  return `${path}|${exact}|${stableStringify(pathVariable ?? {})}|${stableStringify(querystring ?? {})}|${name}|${redirect}`;
};

/**
 * 合并路径
 * @param path        子路径
 * @param parentPath  上级路径
 */
const mergePath = (path: string = '', parentPath: string = '/') => {
  if (isUrl(path)) return path;
  if ((path || parentPath).startsWith('/')) return path;
  return `/${parentPath}/${path}`.replace(/\/\//g, '/').replace(/\/\//g, '/');
};

/**
 * 把路由配置转换成菜单数据
 * @param configContext 配置上下文
 * @param current       当前路由配置
 * @param parent        父级菜单配置
 */
const routeToMenu = (configContext: GlobalConfigContext, current: RuntimeRouter, parent?: RuntimeMenuItem): RuntimeMenuItem => {
  const currentMenu: RuntimeMenuItem = { routerConfig: current, menuKey: getMenuKey(current), parentKeys: [], children: [], isHide: false };
  if (parent && parent.menuKey && parent.parentKeys) {
    currentMenu.parentKeys = [...parent.parentKeys, parent.menuKey];
  }
  // 国际化处理
  const enableLocale = current.enableLocale ?? configContext.menuSettings.enableLocale;
  const formatMessage = configContext.formatMessage;
  if (enableLocale && formatMessage) {
    // 菜单名称
    if (current.nameLocale) {
      current.name = formatMessage({ id: current.nameLocale, defaultMessage: current.name });
    }
    // 页面标题
    if (current.pageTitleLocale) {
      current.pageTitle = formatMessage({ id: current.pageTitleLocale, defaultMessage: current.pageTitle });
    }
    // 面包屑文本
    if (current.breadcrumbNameLocale) {
      current.breadcrumbName = formatMessage({ id: current.breadcrumbNameLocale, defaultMessage: current.breadcrumbName });
    }
    // 面包屑的子面包屑文本
    current.breadcrumbChildren.forEach((item) => {
      if (item.breadcrumbNameLocale) {
        item.breadcrumbName = formatMessage({ id: item.breadcrumbNameLocale, defaultMessage: item.breadcrumbName });
      }
    });
    // 菜单分组名称
    if (current.groupNameLocale) {
      current.groupName = formatMessage({ id: current.groupNameLocale, defaultMessage: current.groupName });
    }
  }
  // 根据配置规则，当前菜单是否隐藏
  let isHide = false;
  if (parent && parent.isHide) {
    // 上级菜单隐藏-子菜单一定隐藏
    isHide = true;
  } else if (current.hideMenu) {
    // 需要隐藏当前菜单
    isHide = true;
  } else if (parent && parent.routerConfig.hideChildrenMenu) {
    // 上级菜单设置要隐藏子菜单
    isHide = true;
  }
  currentMenu.isHide = isHide;
  // 处理子菜单
  current.routes?.forEach((childRouter) => {
    // 过滤自动生成的404页面
    // const keys = lodash.keys(childRouter);
    // if (keys.length === 1 && keys[0] === 'component') return;
    // 递归把路由转化成菜单
    const childMenu = routeToMenu(configContext, childRouter, currentMenu);
    currentMenu.children.push(childMenu);
  });
  return currentMenu;
};
/** 使用memoizeOne优化性能 */
const memoizeOneRouteToMenu = memoizeOne(routeToMenu, isEqual);

/**
 * 拍平的菜单数据
 * <pre>
 *   Map<path, RuntimeMenuItem>
 * </pre>
 */
const flattenMenuData = (rootMenu: RuntimeMenuItem): Map<string, RuntimeMenuItem> => {
  // Map用于确保key的顺序
  const flattenMenuMap = new Map<string, RuntimeMenuItem>();
  flattenMenuMap.set(mergePath(rootMenu.routerConfig.path, '/'), rootMenu);
  // 拍平菜单数据
  const flattenMenuArray = (menuArray: RuntimeMenuItem[], parent?: RuntimeMenuItem) => {
    menuArray.forEach((menuItem) => {
      if (!menuItem) return;
      if (menuItem && menuItem.children) {
        // 递归
        flattenMenuArray(menuItem.children, menuItem);
      }
      const fullPath = mergePath(menuItem.routerConfig.path, parent ? parent.routerConfig.path : '/');
      if (!fullPath || lodash.trim(fullPath).length <= 0) return;
      flattenMenuMap.set(fullPath, menuItem);
    });
  };
  if (rootMenu.children) {
    flattenMenuArray(rootMenu.children);
  }
  return flattenMenuMap;
};
/** 使用memoizeOne优化性能 */
const memoizeOneFlattenMenuData = memoizeOne(flattenMenuData, isEqual);

/**
 * 调整菜单数据结构最终变成
 * <pre>
 *   { [fullPath: string]: RuntimeMenuItem; }
 * </pre>
 */
function fromEntries(breadcrumbMap: Map<string, RuntimeMenuItem>) {
  return [...breadcrumbMap].reduce((obj: LayoutMenuData['flattenMenu'], [key, val]) => {
    obj[key] = val;
    return obj;
  }, {});
}

/**
 * 过滤隐藏的菜单
 * @param rootMenu 根菜单
 */
const filterMenuData = (rootMenu: RuntimeMenuItem): RuntimeMenuItem | undefined => {
  if (rootMenu.routerConfig.hideMenu) return undefined;
  if (rootMenu.routerConfig.hideChildrenMenu) {
    const { children, ...rootProps } = rootMenu;
    return { ...rootProps, children: [] };
  }
  const showChildren: RuntimeMenuItem[] = [];
  rootMenu.children.forEach((item) => {
    // 递归过滤隐藏的菜单
    const child = filterMenuData(item);
    if (child) showChildren.push(child);
  });
  if (showChildren.length > 0) rootMenu.children = showChildren;
  return rootMenu;
};

/**
 * 获取菜单数据
 * @param configContext 配置上下文
 * @param location      location 信息
 * @param rootRouter    根路由配置
 */
const getLayoutMenuData = (configContext: GlobalConfigContext, location: UmiLocation, rootRouter: RuntimeRouter): LayoutMenuData => {
  // 把路由配置转换成菜单数据
  const rootMenu = memoizeOneRouteToMenu(configContext, rootRouter);
  // 拍平的菜单数据
  const flattenMenuMap = memoizeOneFlattenMenuData(rootMenu);
  // 处理拍平的菜单数据-变成Object
  const flattenMenu = fromEntries(flattenMenuMap);
  // 过滤隐藏的菜单
  const rootShowMenu = filterMenuData(rootMenu);
  // 当前访问Url地址
  const currentPath = location.pathname ?? '/';
  // 当前访问页面对应的菜单
  const currentMenu = pathMatchMenu(flattenMenuMap, currentPath);
  // 当前访问页面对应的显示菜单(显示逻辑对应关系)
  let showCurrentMenu = currentMenu;
  if (currentMenu && currentMenu.isHide) {
    // 详情页
    const pathArray = pathToList(currentMenu.routerConfig.path).reverse();
    if (pathArray?.length > 0) pathArray.shift();
    // console.log("pathArray -> ", pathArray);
    let currentMenuTmp: RuntimeMenuItem | undefined = undefined;
    pathArray.forEach((path) => {
      if (currentMenuTmp) return;
      currentMenuTmp = pathMatchMenu(flattenMenuMap, path);
      // 菜单隐藏或者菜单存在子菜单则不能选中
      if (currentMenuTmp?.routerConfig.hideMenu || (currentMenuTmp?.routerConfig.routes && currentMenuTmp?.routerConfig.routes?.length > 0)) {
        currentMenuTmp = undefined;
      }
    });
    showCurrentMenu = currentMenuTmp;
  }
  return { rootMenu, showRootMenu: rootShowMenu, flattenMenuMap, flattenMenu, currentPath, currentMenu, showCurrentMenu };
};

export { GlobalConfigContext, getLayoutMenuData };
