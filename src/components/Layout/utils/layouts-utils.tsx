import React, { CSSProperties } from 'react';
import { compile, pathToRegexp } from 'path-to-regexp';
import qs, { stringify } from 'qs';
import lodash from 'lodash';
import classNames from 'classnames';
import stableStringify from 'fast-json-stable-stringify';
import { Menu } from 'antd';
import { umiHistory } from '@/common/utils';
import AntdIcon, { AntdIconFont, createIconFontCN } from '@/components/AntdIcon';
import styles from '../GlobalSide/SideFirstMenu.less';
import { BreadcrumbRoute } from './breadcrumb';

/**
 * 根据url path匹配到对应的RuntimeMenuItem
 * @param flattenMenuMap  拍平的菜单数据 Map&lt;RuntimeMenuItem.routerConfig.path, RuntimeMenuItem&gt;
 * @param path            url path
 */
const pathMatchMenu = (flattenMenuMap: Map<String, RuntimeMenuItem>, path: string): RuntimeMenuItem | undefined => {
  let currentMenu: RuntimeMenuItem | undefined = flattenMenuMap.get(path);
  if (currentMenu) return currentMenu;
  flattenMenuMap.forEach((menu, varPath) => {
    if (currentMenu) return;
    if (pathToRegexp(varPath.replace('?', '')).test(path)) {
      currentMenu = menu;
    }
  });
  return currentMenu;
};

const urlReg = /(((^https?:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:\/[+~%/.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
/** 判断字符串是完整url */
const isUrl = (path: string): boolean => urlReg.test(path);

/**
 * 把path转换成数组
 * /userInfo/2144/id => ['/userInfo','/userInfo/2144,'/userInfo/2144/id']
 * @param path URL路径
 */
const pathToList = (path?: string): string[] => {
  if (!path || path === '/') return ['/'];
  const pathList = path.split('/').filter((i) => i);
  return pathList.map((_, index) => `/${pathList.slice(0, index + 1).join('/')}`);
};

/**
 * 获取页面标题
 * @param layoutMenuData  全局Layout菜单数据
 * @param htmlTitleSuffix html页面title后缀
 */
const getHtmlTitle = (layoutMenuData: LayoutMenuData, htmlTitleSuffix?: string): string => {
  let title = 'Ant Design Pro';
  const { flattenMenuMap, currentPath } = layoutMenuData;
  const menu = pathMatchMenu(flattenMenuMap, currentPath);
  if (menu) {
    if (menu.routerConfig.pageTitle && htmlTitleSuffix) {
      title = `${menu.routerConfig.pageTitle} - ${htmlTitleSuffix ?? ''}`;
    } else if (menu.routerConfig.pageTitle) {
      title = menu.routerConfig.pageTitle;
    } else if (htmlTitleSuffix) {
      title = htmlTitleSuffix;
    }
  } else if (htmlTitleSuffix) {
    title = htmlTitleSuffix;
  }
  return title;
};

/** 页面跳转参数 */
export interface PageJumpForLocationParam {
  /** 跳转的 location */
  newLocation: UmiLocation;
  /** 当前 location */
  currentLocation: UmiLocation;
}

/** 把UmiLocation转换成唯一字符串 */
const getUmiLocationKey = (location: UmiLocation): string => {
  const { pathname, state } = location;
  let { search, hash } = location;
  search = search && search.startsWith('?') ? search.substring(1) : search;
  hash = hash && hash.startsWith('#') ? hash.substring(1) : hash;
  return `${pathname}|${search}|${hash}|${stableStringify(state ?? {})}`;
};

/**
 * 根据Location进行跳转页面
 * @param params                跳转参数
 * @param pageJumpBeforeCalBack 跳转前的回调函数
 */
const pageJumpForLocation = (params: PageJumpForLocationParam, pageJumpBeforeCalBack?: Function): boolean => {
  const { newLocation, currentLocation } = params;
  const newKey = getUmiLocationKey(newLocation);
  const currentKey = getUmiLocationKey(currentLocation);
  if (newKey === currentKey) {
    return false;
  }
  if (pageJumpBeforeCalBack instanceof Function) {
    pageJumpBeforeCalBack();
  }
  umiHistory.push({ pathname: newLocation.pathname, search: qs.stringify(newLocation.query), state: newLocation.state, query: newLocation.query });
  return true;
};

/** 页面跳转参数 */
export interface PageJumpForMenuParam {
  /** 跳转的 RouterConfig */
  router: {
    /** 路由路径，可以被 path-to-regexp@^1.7.0 理解的路径通配符 */
    path: string;
    /** 配置路由跳转(前端路由跳转) */
    redirect?: string;
    /** 菜单路径中有变量时，配置的路径变量对象 */
    pathVariable?: PathVariable;
    /** 设置菜单路径的queryString部分 */
    querystring?: QueryString;
  };
  /** 当前 location */
  currentLocation: UmiLocation;
}

/**
 * 根据RuntimeRouter进行跳转页面
 * @param params                跳转参数
 * @param pageJumpBeforeCalBack 跳转前的回调函数
 */
const pageJumpForRouter = (params: PageJumpForMenuParam, pageJumpBeforeCalBack?: Function): boolean => {
  const { router, currentLocation } = params;
  const { pathname: currentPathname } = currentLocation;
  let { search: currentSearch, hash: currentHash } = currentLocation;
  currentSearch = currentSearch && currentSearch.startsWith('?') ? currentSearch.substring(1) : currentSearch;
  currentHash = currentHash && currentHash.startsWith('#') ? currentHash.substring(1) : currentHash;
  let currentUrlPath = currentPathname;
  if (currentSearch && currentSearch.length > 0) {
    currentUrlPath = `${currentUrlPath}?${currentSearch}`;
  }
  if (currentHash && currentHash.length > 0) {
    currentUrlPath = `${currentUrlPath}#${currentHash}`;
  }
  let newUrlPath: string | undefined;
  let newPathname: string | undefined;
  if (router.path) {
    newPathname = router.path;
    if (newPathname.indexOf(':') >= 0 && router.pathVariable) {
      newPathname = compile(newPathname)(router.pathVariable);
    }
    newUrlPath = newPathname;
    if (router.querystring) {
      newUrlPath = `${newUrlPath}?${qs.stringify(router.querystring)}`;
    }
  } else if (router.redirect) {
    newUrlPath = router.redirect;
  }
  // console.log("pageJumpForMenu -> ", currentUrlPath, newUrlPath);
  if (!newUrlPath || currentUrlPath === newUrlPath) {
    return false;
  }
  if (pageJumpBeforeCalBack instanceof Function) {
    pageJumpBeforeCalBack();
  }
  umiHistory.push({ pathname: newPathname, search: qs.stringify(router.querystring), query: router.querystring });
  return true;
};

/**
 * 根据 menuKey 获取 RuntimeMenuItem
 * @param flattenMenuMap  拍平的菜单数据
 * @param key             菜单唯一Key(menuKey)
 */
const getMenuItemByKey = (flattenMenuMap: Map<String, RuntimeMenuItem>, key: string): RuntimeMenuItem | undefined => {
  let menuItem: RuntimeMenuItem | undefined = undefined;
  flattenMenuMap.forEach((menu) => {
    if (menuItem) return;
    if (menu.menuKey === key) menuItem = menu;
  });
  return menuItem;
};

/**
 * 获取默认展开的菜单 - 默认实现
 * @param menuDefaultOpen 是否默认展开菜单
 * @param rootMenu        根菜单
 * @param currentMenu     当前菜单
 */
const getDefaultOpenKeys = (menuDefaultOpen: boolean, rootMenu: RuntimeMenuItem, currentMenu: RuntimeMenuItem): string[] => {
  const defaultOpenKeys: string[] = [];
  const setOpenKeys = (menu: RuntimeMenuItem): void => {
    if (!menu || !menu.children || menu.children.length <= 0 || menu.isHide) return;
    const isOpen = menu.routerConfig.defaultOpen ?? menuDefaultOpen;
    if (isOpen && menu.menuKey !== rootMenu.menuKey) {
      defaultOpenKeys.push(menu.menuKey);
    }
    if (isOpen || menu.menuKey === rootMenu.menuKey) {
      menu.children.forEach((childrenMenu) => setOpenKeys(childrenMenu));
    }
  };
  setOpenKeys(rootMenu);
  // 保证当前页面对应的父菜单都是展开状态
  if (currentMenu && currentMenu.parentKeys && currentMenu.parentKeys.length > 0) {
    let breakFlag = true;
    currentMenu.parentKeys.forEach((key) => {
      if (rootMenu.menuKey === key) {
        breakFlag = false;
        return;
      }
      if (breakFlag) return;
      if (defaultOpenKeys.indexOf(key) === -1) {
        defaultOpenKeys.push(key);
      }
    });
  }
  // console.log("getDefaultOpenKeys -> ", defaultOpenKeys);
  return defaultOpenKeys;
};

/**
 * 获取菜单图标
 * @param icon          图标字符串
 * @param iconScriptUrl 图标字体 - iconfont.cn项目在线生成的js(地址: https://www.iconfont.cn/)
 */
const getMenuIcon = (icon?: string, iconScriptUrl?: string): React.ReactNode | undefined => {
  if (!icon) return undefined;
  // 自定义字体图标
  let IconFont: AntdIconFont | undefined;
  if (iconScriptUrl) {
    IconFont = createIconFontCN({
      scriptUrl: iconScriptUrl,
    });
  }
  let iconNode: React.ReactNode | undefined;
  const style: CSSProperties = { marginRight: 8 };
  if (IconFont && icon.startsWith('icon-')) {
    iconNode = <IconFont type={icon} style={style} />;
  } else {
    iconNode = <AntdIcon type={icon} style={style} />;
  }
  return iconNode;
};

/**
 * 创建新的多标签页数据
 * @param menuData    菜单数据
 * @param location    当前location
 * @param currentPath 当前url path
 */
const newMultiTabItem = (menuData: RuntimeMenuItem, location: UmiLocation, currentPath: string): MultiTabItem => {
  const { pathname } = location;
  let { search, hash } = location;
  search = search && search.startsWith('?') ? search.substring(1) : search;
  hash = hash && hash.startsWith('#') ? hash.substring(1) : hash;
  const multiTabKey = `${menuData.menuKey}|${pathname}|${search}|${hash}`;
  // console.log("location -> ", location, multiTabKey);
  // console.log("location -> ", stringify(location));
  return {
    menuItem: menuData,
    multiTabKey,
    currentPath,
    location,
    isHomePage: false,
    active: false,
    lastActiveTime: new Date().getTime(),
    showClose: true,
  };
};

/**
 * 获取当前一级菜单的Key
 * @param layoutMenuData  全局Layout菜单数据
 */
const getCurrentFirstMenuKey = (layoutMenuData: LayoutMenuData): string | undefined => {
  const { currentMenu } = layoutMenuData;
  // console.log("getCurrentFirstMenuKey", currentMenu);
  let currentFirstMenuKey: string | undefined;
  if (currentMenu && currentMenu.parentKeys) {
    if (currentMenu.parentKeys.length >= 2) {
      currentFirstMenuKey = currentMenu.parentKeys[1];
    } else if (currentMenu.parentKeys.length === 1) {
      currentFirstMenuKey = currentMenu.menuKey;
    }
  }
  return currentFirstMenuKey;
};

/**
 * 获取当前一级菜单
 * @param layoutMenuData  全局Layout菜单数据
 */
const getCurrentFirstMenu = (layoutMenuData: LayoutMenuData): RuntimeMenuItem | undefined => {
  const { flattenMenuMap } = layoutMenuData;
  const currentFirstMenuKey = getCurrentFirstMenuKey(layoutMenuData);
  if (!currentFirstMenuKey) return undefined;
  let currentFirstMenu: RuntimeMenuItem | undefined = undefined;
  flattenMenuMap.forEach((menu) => {
    if (currentFirstMenu) return;
    if (menu.menuKey === currentFirstMenuKey) currentFirstMenu = menu;
  });
  return currentFirstMenu;
};

/**
 * 获取第一个显示的页面菜单
 * @param menu 菜单数据
 */
const getFirstShowMenu = (menu: RuntimeMenuItem): RuntimeMenuItem | undefined => {
  if (!menu.children) return menu;
  let showMenu: RuntimeMenuItem | undefined = undefined;
  menu.children.forEach((childrenMenu) => {
    if (showMenu) return;
    if (childrenMenu.isHide) return;
    if (childrenMenu.children && childrenMenu.children.length > 0) {
      // 递归获取
      showMenu = getFirstShowMenu(childrenMenu);
    } else {
      showMenu = childrenMenu;
    }
  });
  return showMenu;
};

/**
 * 获取第一个页面菜单
 * @param menu 菜单数据
 */
const getFirstMenu = (menu: RuntimeMenuItem): RuntimeMenuItem => {
  if (!menu.children || menu.children.length <= 0) return menu;
  let showMenu: RuntimeMenuItem = menu;
  menu.children.forEach((childrenMenu) => {
    if (showMenu) return;
    // 递归获取
    showMenu = getFirstMenu(childrenMenu);
  });
  return showMenu;
};

/**
 * 过滤菜单数据
 * @param menuData    菜单数据
 * @param searchValue 过滤关键字
 */
const getSideMenuData = (menuData: RuntimeMenuItem, searchValue: string): RuntimeMenuItem | undefined => {
  // console.log("getSideMenuData ", menuData, searchValue);
  if (searchValue === undefined || searchValue === null || lodash.trim(searchValue).length <= 0) return menuData;
  const searchKey = searchValue.toLocaleLowerCase();
  const showMenuKeys: Set<string> = new Set<string>([menuData.menuKey]);
  // 过滤 - 收集匹配的 menu.key
  const filterSearch = (menu: RuntimeMenuItem): void => {
    if (menu.routerConfig.name && menu.routerConfig.name.toLocaleLowerCase().includes(searchKey)) {
      showMenuKeys.add(menu.menuKey);
      if (menu.parentKeys) {
        menu.parentKeys.forEach((item) => showMenuKeys.add(item));
      }
    }
    if (menu.children && menu.children.length > 0) {
      menu.children.forEach((item) => filterSearch(item));
    }
  };
  filterSearch(menuData);
  if (showMenuKeys.size <= 1) return undefined;
  const resMenu: RuntimeMenuItem = { ...menuData, children: [] };
  const filterKey = (oldMenu: RuntimeMenuItem, newParentMenu: RuntimeMenuItem): void => {
    let menu: RuntimeMenuItem | undefined;
    if (showMenuKeys.has(oldMenu.menuKey)) {
      // 构造子菜单
      menu = { ...oldMenu, children: [] };
      // if (!oldMenu.children) delete menu.children;
      // 加入子菜单
      if (!newParentMenu.children) newParentMenu.children = [];
      newParentMenu.children.push(menu);
    }
    if (menu && oldMenu.children && oldMenu.children.length > 0) {
      const parentMenu: RuntimeMenuItem = menu;
      oldMenu.children.forEach((item) => filterKey(item, parentMenu));
    }
  };
  if (menuData.children && menuData.children.length > 0) {
    menuData.children.forEach((oldMenu) => filterKey(oldMenu, resMenu));
  }
  // console.log("getSideMenuData -> ", showMenuKeys, resMenu);
  return resMenu;
};

/**
 * 计算首页
 * @param currentFirstMenu 当前的一级菜单
 */
const getHomeMultiTabItem = (currentFirstMenu: RuntimeMenuItem): MultiTabItem | undefined => {
  if (!currentFirstMenu.children || currentFirstMenu.children.length <= 0) {
    return undefined;
  }
  let homeMenu: RuntimeMenuItem | undefined = undefined;
  const traverseFirstMenu = (arrayMenu: RuntimeMenuItem[]) => {
    if (!arrayMenu) return;
    if (homeMenu) return;
    arrayMenu.forEach((menu) => {
      if (homeMenu) return;
      if (!menu.routerConfig.routes || menu.routerConfig.routes.length <= 0) {
        homeMenu = menu;
      }
      // 递归
      if (menu.children) traverseFirstMenu(menu.children);
    });
  };
  traverseFirstMenu(currentFirstMenu.children);
  if (!homeMenu) return undefined;
  const location: UmiLocation = {
    pathname: homeMenu!.routerConfig.path || '/',
    search: stringify(homeMenu!.routerConfig.querystring),
    hash: '',
    query: homeMenu!.routerConfig.querystring || {},
    state: {},
  };
  const homeTab = newMultiTabItem(homeMenu, location, location.pathname);
  homeTab.isHomePage = true;
  homeTab.showClose = false;
  return homeTab;
};

/**
 * 面包屑渲染
 * @param route     当前需要渲染的面包屑
 * @param params    扩展参数params
 * @param routes    面包屑路由数组
 * @param paths     当前需要渲染的面包屑路径数组
 * @param location  当前UmiLocation数据
 */
const breadcrumbItemRender = (route: BreadcrumbRoute, params: any, routes: BreadcrumbRoute[], paths: string[], location: UmiLocation): React.ReactNode => {
  // console.log("defaultItemRender ->", route);
  const last = routes.indexOf(route) === routes.length - 1;
  if (last || !route.isPage) {
    const style: CSSProperties = {};
    if (route.children && route.children.length > 0) {
      style.cursor = 'pointer';
    }
    return <span style={style}>{route.breadcrumbName}</span>;
  }
  return <a onClick={() => pageJumpForRouter({ currentLocation: location, router: route })}>{route.breadcrumbName}</a>;
};

/**
 * 默认的菜单项渲染(渲染叶子菜单)
 * @param menu  菜单数据
 * @param icon  菜单图标
 */
const defaultMenuItemRender = (menu: RuntimeMenuItem, icon?: React.ReactNode): React.ReactNode => {
  return (
    <>
      {icon}
      <span>{menu.routerConfig.name}</span>
    </>
  );
};

/**
 * 自定义菜单渲染(渲染叶子菜单)
 * @param menu  菜单数据
 * @param icon  菜单图标
 */
const defaultCustomMenuItemRender = (menu: RuntimeMenuItem, icon?: React.ReactNode): React.ReactNode => {
  return (
    <>
      {icon}
      <span className={classNames(styles.customMenuItemTitle)}>{menu.routerConfig.name}</span>
    </>
  );
};

interface GetMenuNodeParam {
  /** 根菜单(过滤隐藏的菜单) */
  showRootMenu: RuntimeMenuItem;
  /** 自定义菜单图标字体 - iconfont.cn项目在线生成的js(地址: https://www.iconfont.cn/) */
  menuIconScriptUrl?: string;
  /** 自定义渲染菜单项 */
  menuItemRender?: (menu: RuntimeMenuItem, icon?: React.ReactNode) => React.ReactNode;
  /** 自定义菜单项class样式 */
  /** 自定义菜单项class样式 */
  menuItemClassName?: string;
  /** 自定义菜单项样式 */
  menuItemStyle?: CSSProperties;
}

/**
 * 获取 Antd 菜单节点(Menu.Item)
 * @param param 相关参数
 */
const getAntdMenuItems = (param: GetMenuNodeParam): React.ReactNode[] => {
  const { showRootMenu, menuIconScriptUrl, menuItemRender, menuItemClassName, menuItemStyle = {} } = param;
  const nodes: React.ReactNode[] = [];
  if (showRootMenu && showRootMenu.children && showRootMenu.children.length > 0) {
    showRootMenu.children.forEach((menu) => {
      // 菜单图标
      const icon = getMenuIcon(menu.routerConfig.icon, menuIconScriptUrl);
      // 菜单内容
      const node = (
        <Menu.Item className={menuItemClassName} style={menuItemStyle} key={menu.menuKey} disabled={false} data-menu={menu}>
          {menuItemRender instanceof Function ? menuItemRender(menu, icon) : defaultMenuItemRender(menu, icon)}
        </Menu.Item>
      );
      nodes.push(node);
    });
  }
  // console.log("getMenuNode -> ", nodes);
  return nodes;
};

export {
  pathMatchMenu,
  pathToList,
  isUrl,
  getHtmlTitle,
  pageJumpForLocation,
  pageJumpForRouter,
  getMenuItemByKey,
  getDefaultOpenKeys,
  getMenuIcon,
  newMultiTabItem,
  getCurrentFirstMenuKey,
  getCurrentFirstMenu,
  getFirstShowMenu,
  getFirstMenu,
  getSideMenuData,
  getHomeMultiTabItem,
  breadcrumbItemRender,
  defaultMenuItemRender,
  defaultCustomMenuItemRender,
  getAntdMenuItems,
};
