import { Route } from 'antd/es/breadcrumb/Breadcrumb';
import { pathMatchMenu, pathToList } from './layouts-utils';

/** 页面包屑Route数据 */
interface BreadcrumbRoute extends Route {
  /** 跳转路径 */
  path: string;
  /** 菜单路径中有变量时，配置的路径变量对象 */
  pathVariable?: PathVariable;
  /** 设置菜单路径的querystring部分 */
  querystring?: QueryString;
  /** 面包屑名称 */
  breadcrumbName: string;
  /** 当前面包屑是否对应一个页面 */
  isPage?: boolean;
  /** 当前层级面包屑的下拉面包屑 */
  children?: Omit<BreadcrumbRoute, 'children'>[];

  /** 扩展属性 */
  [key: string]: any;
}

/** 把菜单对象转换成面包屑对象 */
const menuToBreadcrumbRoute = (menu: RuntimeMenuItem): BreadcrumbRoute => {
  const { routerConfig } = menu;
  const route: BreadcrumbRoute = {
    path: routerConfig.path ?? '/',
    pathVariable: routerConfig.pathVariable,
    querystring: routerConfig.querystring,
    isPage: !routerConfig.routes || routerConfig.routes.length <= 0,
    breadcrumbName: routerConfig.breadcrumbName ?? '',
  };
  if (routerConfig.breadcrumbChildren && routerConfig.breadcrumbChildren.length > 0) {
    routerConfig.breadcrumbChildren.forEach((item) => {
      if (!item.redirectPath || !item.breadcrumbName) return;
      if (!route.children) route.children = [];
      route.children.push({
        path: item.redirectPath,
        pathVariable: item.pathVariable,
        querystring: item.querystring,
        breadcrumbName: item.breadcrumbName,
        isPage: true,
      });
    });
  }
  return route;
};

/**
 * 获取面包屑路由数据(根据umi路由配置动态生成) - BreadcrumbProps["routes"]
 */
const getBreadcrumbRoutes = (layoutMenuData: LayoutMenuData): BreadcrumbRoute[] => {
  const { flattenMenuMap, currentPath } = layoutMenuData;
  const breadcrumbRoutes: BreadcrumbRoute[] = [];
  const currentMenu = layoutMenuData.currentMenu ? layoutMenuData.currentMenu : pathMatchMenu(flattenMenuMap, currentPath);
  // 未找到匹配菜单
  if (!currentMenu) return [];
  breadcrumbRoutes.push(menuToBreadcrumbRoute(currentMenu));
  const pathArray = pathToList(currentMenu.routerConfig.path).reverse();
  if (pathArray?.length > 0) pathArray.shift();
  let preMenuTmp: RuntimeMenuItem = currentMenu;
  pathArray.forEach((path) => {
    const menuTmp = pathMatchMenu(flattenMenuMap, path);
    if (!menuTmp || menuTmp.routerConfig.hideBreadcrumb) return;
    // console.log("getBreadcrumbRoutes ->", menuTmp.path, menuTmp.routes, preMenuTmp.routes, preMenuTmp.isHide);
    if (menuTmp.routerConfig.routes && menuTmp.routerConfig.routes.length > 0 && !menuTmp.isHide) {
      // 有子菜单 && 自己未隐藏
      breadcrumbRoutes.push(menuToBreadcrumbRoute(menuTmp));
    } else if (
      (!menuTmp.routerConfig.routes || menuTmp.routerConfig.routes.length <= 0) &&
      (!preMenuTmp.routerConfig.routes || preMenuTmp.routerConfig.routes.length <= 0) &&
      preMenuTmp.isHide
    ) {
      // 无子菜单 && 下级菜单也无子菜单 && 下级菜单隐藏
      breadcrumbRoutes.push(menuToBreadcrumbRoute(menuTmp));
    }
    preMenuTmp = menuTmp;
  });
  return breadcrumbRoutes.reverse();
};

export { BreadcrumbRoute, getBreadcrumbRoutes };
