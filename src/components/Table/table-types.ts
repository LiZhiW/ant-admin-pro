import React, { CSSProperties, ReactNode } from 'react';
import { LinkProps } from 'react-router-dom';
import { ModalFuncProps } from 'antd/es/modal';
import { PopconfirmProps } from 'antd/es/popconfirm';
import { AvatarProps } from 'antd/es/avatar';
import { BadgeProps } from 'antd/es/badge';
import { PopoverProps } from 'antd/es/popover';
import { AbstractTooltipProps, RenderFunction } from 'antd/es/tooltip';
import { TagProps } from 'antd/es/tag';
import { ProgressProps } from 'antd/es/progress';
import { RateProps } from 'antd/es/rate';
import { CheckboxGroupProps, CheckboxProps } from 'antd/es/checkbox';
import { AbstractCheckboxProps } from 'antd/es/checkbox/Checkbox';
import { RadioGroupProps } from 'antd/es/radio';
import { RadioChangeEvent } from 'antd/es/radio/interface';
import { SwitchProps } from 'antd/es/switch';
import { SliderRangeProps, SliderSingleProps } from 'antd/es/slider';
import { ColumnProps } from 'antd/es/table';
import { SortOrder } from 'antd/es/table/interface';
import { EnumItemLabel, EnumItemValue, MapperData } from '@/common';
import { DataTable } from './DataTable';

/** 表格数据行类型 */
type TableRowData = any;

/** 对话框属性 */
interface ModalDialogProps extends ModalFuncProps {
  /** 对话框类型 */
  method: 'info' | 'success' | 'error' | 'warning' | 'confirm';
}

/** 操作项操作类型 */
enum ActionType {
  /** 跳转链接 */
  Link,
  /** 直接调用对话框方法(info、success、error、warning、confirm) - 推荐 */
  ModalDialog,
  /** 气泡确认框交互(当操作项在“更多”子菜单中时不生效) */
  PopConfirm,
}

/** 表格操作列的操作项定义 */
interface TableColumnAction {
  /** 当前操作项顺序，从左到右(从小到大) */
  order?: number;
  /** 操作项自定义样式 */
  actionStyle?: CSSProperties;
  /** 操作项自定义扩展属性 */
  actionExtProps?: object;
  /** 操作项图标 */
  actionIcon?: ReactNode | ((record: TableRowData) => ReactNode);
  /** 操作项文本 */
  actionText: ReactNode | ((record: TableRowData) => ReactNode);
  /** 是否在当前操作后添加Divider(分隔栏) */
  appendDivider?: boolean;
  /** 操作类型 */
  actionType?: ActionType;
  /** 获取跳转链接属性 */
  getLinkProps?: LinkProps<any> | ((record: TableRowData, dataTable: DataTable) => LinkProps<any>);
  /** 直接调用对话框属性 */
  getModalDialogProps?: ModalDialogProps | ((record: TableRowData, dataTable: DataTable) => ModalDialogProps);
  /** 气泡确认框属性 */
  getPopConfirmProps?: PopconfirmProps | ((record: TableRowData, dataTable: DataTable) => PopconfirmProps);
  /** 取消操作事件 */
  onCancel?: (record: TableRowData, dataTable: DataTable, event: any) => void | Promise<void>;
  /** 确认操作事件 */
  onConfirm?: (record: TableRowData, dataTable: DataTable, event: any) => boolean | Promise<boolean>;
  /** 完全自定义处理操作项触发逻辑 */
  onAction?: (record: TableRowData, dataTable: DataTable, event: any) => void | Promise<void>;
}

/** 数据值类型 */
enum DataValueType {
  /** 数字 */
  Number,
  /** 金额 */
  Money,
  /** 日期，如：2020-05-15 */
  Date,
  /** 日期时间，如：2020-05-15 09:33:09 */
  DateTime,
  /** 时间，如：09:33:09 */
  Time,
  /**
   * 日期范围，字段数据是一个数组，长度是2<br />
   * 如：2020-05-12 ~ 2020-05-15<br />
   * 如：2020-05-12 ~ ?<br />
   * 如：? ~ 2020-05-15<br />
   * 如：-<br />
   */
  DateRange,
  /**
   * 日期范围，字段数据是一个数组，长度是2<br />
   * 如：2020-05-12 09:33:09 ~ 2020-05-15 11:18:29<br />
   * 如：2020-05-12 09:33:09 ~ ?<br />
   * 如：? ~ 2020-05-15 11:18:29<br />
   * 如：-<br />
   */
  DateTimeRange,
  /**
   * 数据时间值与当前时间的距离时间，支持数据类型："Date(时间字符串)" | "数字(数字字符串)" <br />
   * 如：5分钟前<br />
   * 如：3秒前<br />
   * 如：36分钟58秒前<br />
   */
  TimeBefore,
  /**
   * 时间毫秒数转成易读的时间，支持数据类型："Date(时间字符串)" | "数字(数字字符串)" <br />
   * 如：1ms<br />
   * 如：1s2ms<br />
   * 如：1h27m53s<br />
   */
  HowLongTime,
  /** 比特大小转易读的数据大小格式，如：2.3MB */
  DataBytesSize,
}

/** 数据值包装显示的组件类型 */
enum DataWrapperType {
  /** 映射数据类型，MapperData */
  Mapper,
  /** 头像 */
  Avatar,
  /** 徽标数 */
  Badge,
  /** 气泡卡片 */
  Popover,
  /** 文字提示 */
  Tooltip,
  /** 标签 */
  Tag,
  /** 进度条 */
  Progress,
  /** 气泡确认框 */
  PopConfirm,
  /** 跳转链接 */
  Link,
  /** 对话框 */
  ModalDialog,
  /** 多选框 */
  Checkbox,
  /** 多选组 */
  CheckboxGroup,
  /** 评分组件 */
  Rate,
  /** 单选框 */
  Radio,
  /** 单选组 */
  RadioGroup,
  /** 开关 */
  Switch,
  /** 滑动条 */
  Slider,
}

/** 基础的MapperData */
interface BasePropsItem {
  /** 数据值 */
  value: EnumItemValue;
  /** 数据显示值 */
  label?: EnumItemLabel;
}

/** 头像 */
interface AvatarPropsExt extends BasePropsItem, AvatarProps {}

/** 徽标数 */
interface BadgePropsExt extends BasePropsItem, BadgeProps {}

/** 气泡卡片 */
interface PopoverPropsExt extends BasePropsItem, PopoverProps {
  children?: React.ReactElement;
}

/** 文字提示 */
interface TooltipPropsExt extends BasePropsItem, AbstractTooltipProps {
  title?: React.ReactNode | RenderFunction;
  overlay?: React.ReactNode | RenderFunction;
  children?: React.ReactElement;
}

/** Tag标签 */
interface TagPropsExt extends BasePropsItem, TagProps {}

/** 进度条 */
interface ProgressPropsExt extends BasePropsItem, ProgressProps {}

/** 气泡确认框 */
interface PopConfirmPropsExt extends BasePropsItem, PopconfirmProps {
  children?: React.ReactElement;
}

/** 超链接 */
interface LinkPropsExt<T = any> extends BasePropsItem, LinkProps<T> {
  children?: React.ReactNode;
}

/** 模态对话框 */
interface ModalDialogPropsExt extends BasePropsItem, ModalDialogProps {
  children?: React.ReactNode;
}

/** 多选框 */
interface CheckboxPropsExt extends BasePropsItem, CheckboxProps {
  value: any;
  label?: boolean;
}

/** 多选框组 */
interface CheckboxGroupPropsExt extends BasePropsItem, Omit<CheckboxGroupProps, 'value'> {
  // value: CheckboxValueType[];
  // children?: React.ReactNode[];
  // label?: CheckboxValueType[];
}

/** 评分 */
interface RatePropsExt extends BasePropsItem, Omit<RateProps, 'value'> {
  /** 评分值 */
  label: number;
}

/** 单选框 */
interface RadioPropsExt extends BasePropsItem, AbstractCheckboxProps<RadioChangeEvent> {
  value: any;
  /** 单选是否选中 */
  label?: boolean;
}

/** 单选框组 */
interface RadioGroupPropsExt extends BasePropsItem, RadioGroupProps {
  value: any;
  // label?: boolean;
}

/** Switch开关 */
interface SwitchPropsExt extends BasePropsItem, SwitchProps {
  label?: boolean;
}

type AntdSliderProps = (SliderSingleProps & React.RefAttributes<unknown>) | (SliderRangeProps & React.RefAttributes<unknown>);

/** 滑动输入条 */
interface SliderPropsExt extends BasePropsItem, Omit<AntdSliderProps, 'value'> {
  label?: number;
}

/** 表格列配置 */
interface TableColumn extends ColumnProps<TableRowData> {
  /** 当前列的顺序，从左到右(从小到大) */
  order?: number;
  /** 把列数据转换成文本，用于搜索过滤 */
  renderText?: (originalValue: any, value: any) => string;
  /** 值类型 */
  valueType?: DataValueType;
  /**
   * 自定义格式化字符串<br />
   * Number格式化使用 numeral -->  http://numeraljs.com/ <br />
   * Date(Time、DateTime、DateRange等)格式化使用 moment -->  http://momentjs.cn/docs/#/use-it/ <br />
   */
  formatter?: string;
  /** TimeBefore格式化使用 */
  timeBeforeFmt?: {
    /** 需要显示的层级(默认1) */
    level?: number;
    /** 转换的最大时间差距(默认7天) */
    maxTimeStamp?: number;
    /** 时间单位数组，默认值 ["年", "个月", "天", "小时", "分钟", "秒"] */
    timeUnit?: string[];
  };
  /** HowLongTime格式化使用 */
  howLongTimeFmt?: {
    /** 需要显示的层级(默认3) */
    level?: number;
    /** 时间单位数组，默认值 ["Y", "M", "D", "h", "m", "s", "ms"] */
    timeUnit?: string[];
  };
  /** 值的包装组件类型 */
  wrapperType?: DataWrapperType;
  /** 使用Mapper时的映射数据 */
  mapperData?: MapperData;
  /** 获取Avatar(头像)组件属性 */
  getAvatarProps?: Array<AvatarPropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<AvatarPropsExt>);
  /** 获取Badge(徽标数)组件属性 */
  getBadgeProps?: Array<BadgePropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<BadgePropsExt>);
  /** 获取Popover(气泡卡片)组件属性 */
  getPopoverProps?: Array<PopoverPropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<PopoverPropsExt>);
  /** 获取Popover(文字提示)组件属性 */
  getTooltipProps?: Array<TooltipPropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<TooltipPropsExt>);
  /** 获取Tag(标签)组件属性 */
  getTagProps?: Array<TagPropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<TagPropsExt>);
  /** 获取Progress(进度条)组件属性 */
  getProgressProps?: Array<ProgressPropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<ProgressPropsExt>);
  /** 获取PopConfirm(气泡确认框)组件属性 */
  getPopConfirmProps?: Array<PopConfirmPropsExt> | ((originalValue: any, value: any, record: TableRowData) => Partial<PopConfirmPropsExt>);
  /** 获取Link(跳转链接)组件属性 */
  getLinkProps?: Array<LinkPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => LinkPropsExt);
  /** 获取ModalDialog(对话框)组件属性 */
  getModalDialogProps?: Array<ModalDialogPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => ModalDialogPropsExt);
  /** 获取Checkbox(多选框)组件属性 */
  getCheckboxProps?: Array<CheckboxPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => CheckboxPropsExt);
  /** 获取CheckboxGroup(多选组)组件属性 */
  getCheckboxGroupProps?: Array<CheckboxGroupPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => CheckboxGroupProps);
  /** 获取Rate(评分组件)组件属性 */
  getRateProps?: Array<RatePropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => RatePropsExt);
  /** 获取Radio(单选框)组件属性 */
  getRadioProps?: Array<RadioPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => RadioPropsExt);
  /** 获取RadioGroup(单选组)组件属性 */
  getRadioGroupProps?: Array<RadioGroupPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => RadioGroupProps);
  /** 获取Switch(开关)组件属性 */
  getSwitchProps?: Array<SwitchPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => SwitchPropsExt);
  /** 获取Slider(滑动条)组件属性 */
  getSliderProps?: Array<SliderPropsExt> | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => SliderPropsExt);
  /** 内容前缀 */
  prefix?: ReactNode | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => ReactNode);
  /** 内容后缀 */
  suffix?: ReactNode | ((originalValue: any, value: any, record: TableRowData, dataTable: DataTable) => ReactNode);
}

// align	设置列的对齐方式
// ellipsis	超过宽度将自动省略
// className	列样式类名
// colSpan	表头列合并,设置为 0 时，不渲染
// dataIndex	列数据在数据项中对应的路径，支持通过数组查询嵌套路径
// defaultFilteredValue	默认筛选值
// defaultSortOrder	默认排序顺序
// filterDropdown	可以自定义筛选菜单，此函数只负责渲染图层，需要自行编写各种交互
// filterDropdownVisible	用于控制自定义筛选菜单是否可见
// filtered	标识数据是否经过过滤，筛选图标会高亮
// filteredValue	筛选的受控属性，外界可用此控制列的筛选状态，值为已筛选的 value 数组
// filterIcon	自定义 filter 图标
// filterMultiple	是否多选
// filters	表头的筛选菜单项
// fixed	（IE 下无效）列是否固定，可选 true(等效于 left) 'left' 'right'
// key	React 需要的 key
// render	生成复杂数据的渲染函数
// sorter	排序函数
// sortOrder 排序的受控属性，外界可用此控制列的排序，可设置为 'ascend' 'descend' false
// sortDirections	支持的排序方式，覆盖Table中sortDirections， 取值为 'ascend' 'descend'
// title	列头显示文字
// width	列宽度
// onCell	设置单元格属性
// onFilter	本地模式下，确定筛选的运行函数
// onFilterDropdownVisibleChange	自定义筛选菜单可见变化时调用
// onHeaderCell	设置头部单元格属性
// showSorterTooltip	表头显示下一次排序的 tooltip 提示, 覆盖 table 中showSorterTooltip

/** 重新加载数据模式 */
enum ReloadModel {
  /** 跳转到第一页 */
  ToFirst,
  /** 保持当前页 */
  Keep,
  /** 改变分页 */
  Pagination,
}

/** 重新加载数据参数 */
interface ReloadParams {
  /** 重新加载页面模式 */
  model: ReloadModel;
  /** 页大小 */
  pageSize?: number;
  /** 页码数 */
  pageNo?: number;
  /** 设置排序 */
  sortOrderColumns?: { [column: string]: SortOrder };
}

/** reloadByClient 返回值类型 */
interface GetLocalDataSourceRes {
  /** 数据 */
  dataSource: TableRowData[];
  /** 总条数 */
  total: number;
  /** 当前页 */
  current?: number;
  /** 页大小 */
  pageSize?: number;
  /** 根据返回数据更新组件状态函数 */
  updateState: () => void;
}

export {
  TableRowData,
  ModalDialogProps,
  ActionType,
  TableColumnAction,
  DataValueType,
  DataWrapperType,
  BasePropsItem,
  AvatarPropsExt,
  BadgePropsExt,
  PopoverPropsExt,
  TooltipPropsExt,
  TagPropsExt,
  ProgressPropsExt,
  PopConfirmPropsExt,
  LinkPropsExt,
  ModalDialogPropsExt,
  CheckboxPropsExt,
  CheckboxGroupPropsExt,
  RatePropsExt,
  RadioPropsExt,
  RadioGroupPropsExt,
  SwitchPropsExt,
  AntdSliderProps,
  SliderPropsExt,
  TableColumn,
  ReloadModel,
  ReloadParams,
  GetLocalDataSourceRes,
};
