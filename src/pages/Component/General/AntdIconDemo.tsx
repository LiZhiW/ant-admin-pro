import React, { Component } from 'react';
import { Space } from 'antd';
import { Dispatch } from 'redux';
import AntdIcon, { createIconFontCN } from '@/components/AntdIcon';
import { PageWrapper } from '@/components/Layout';

interface AntdIconDemoProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface AntdIconDemoState {}

class AntdIconDemo extends Component<AntdIconDemoProps, AntdIconDemoState> {
  protected demo1() {
    const fontSize = 32;
    return (
      <Space>
        <AntdIcon icon="PlayCircleOutlined" style={{ fontSize }} />
        <AntdIcon icon="HomeOutlined" style={{ fontSize }} />
        <AntdIcon icon="SmileOutlined" style={{ fontSize }} />
        <AntdIcon icon="SmileOutlined" style={{ fontSize }} rotate={90} />
        <AntdIcon icon="SmileOutlined" style={{ fontSize }} rotate={180} />
        <AntdIcon icon="SmileOutlined" style={{ fontSize }} rotate={270} />
        <AntdIcon icon="LoadingOutlined" style={{ fontSize }} spin={true} />
        <AntdIcon icon="HomeOutlined" style={{ fontSize }} spin={true} />
      </Space>
    );
  }

  protected demo2() {
    const fontSize = 32;
    return (
      <Space>
        <AntdIcon icon="SmileTwoTone" style={{ fontSize }} />
        <AntdIcon icon="HeartTwoTone" twoToneColor="#eb2f96" style={{ fontSize }} />
        <AntdIcon icon="CheckCircleTwoTone" twoToneColor="#52c41a" style={{ fontSize }} />
      </Space>
    );
  }

  protected demo3() {
    const fontSize = 32;
    const IconFont = createIconFontCN({ scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js' });
    return (
      <Space>
        <IconFont type="icon-tuichu" style={{ fontSize }} />
        <IconFont type="icon-facebook" style={{ fontSize }} />
        <IconFont type="icon-twitter" style={{ fontSize }} />
      </Space>
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="AntdIcon(图标)">
        <h3>基本使用</h3>
        {this.demo1()}
        <br />
        <br />
        <h3>双色图标</h3>
        {this.demo2()}
        <br />
        <br />
        <h3>使用 iconfont.cn</h3>
        {this.demo3()}
        <br />
      </PageWrapper>
    );
  }
}

export default AntdIconDemo;
