import React, { Component, CSSProperties } from 'react';
import { Dispatch } from 'redux';
import { Button, Divider, Dropdown, Input, Menu, Radio } from 'antd';
import { ColumnHeightOutlined, DownloadOutlined, DownOutlined, FullscreenOutlined, PlusOutlined, ReloadOutlined, SettingOutlined } from '@ant-design/icons';
import { PageWrapper } from '@/components/Layout';
import { ToolBar } from '@/components/ToolBar';

interface ToolBarDemoProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface ToolBarDemoState {}

class ToolBarDemo extends Component<ToolBarDemoProps, ToolBarDemoState> {
  protected demo1() {
    return (
      <ToolBar
        title="工具栏"
        border={false}
        rightContent={[
          <Input key="1" placeholder="输入提示" />,

          <Button key="2" type="primary" icon={<DownloadOutlined />} />,
          <Button key="3" shape="circle" icon={<DownloadOutlined />} />,
          <Button key="4" shape="round" icon={<DownloadOutlined />} />,
          <Button key="5" shape="round" icon={<DownloadOutlined />}>
            下载
          </Button>,

          <Divider key="6" type="vertical" />,

          <Radio.Group key="7">
            <Radio.Button value="large">Large</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="small">Small</Radio.Button>
          </Radio.Group>,

          <Dropdown
            key="8"
            overlay={
              <Menu>
                <Menu.Item key="1">1st item</Menu.Item>
                <Menu.Item key="2">2nd item</Menu.Item>
                <Menu.Item key="3">3rd item</Menu.Item>
              </Menu>
            }
          >
            <Button>
              Actions <DownOutlined />
            </Button>
          </Dropdown>,
        ]}
      />
    );
  }

  protected demo2() {
    const iconStyle: CSSProperties = { cursor: 'pointer', fontSize: 18, fontWeight: 'bold' };
    return (
      <ToolBar
        title={
          <div key="title" style={{ color: '#FF0033', fontWeight: 'bold' }}>
            自定义标题
          </div>
        }
        rightContent={[
          <Button key="001" type="primary" icon={<PlusOutlined />}>
            新增
          </Button>,
          <Divider key="002" type="vertical" />,
          <Input.Search key="004" placeholder="输入关键字过滤" />,
          <ColumnHeightOutlined key="003" style={iconStyle} />,
          <FullscreenOutlined key="005" style={iconStyle} />,
          <ReloadOutlined key="006" style={iconStyle} />,
          <SettingOutlined key="007" style={iconStyle} />,
        ]}
        rightSpaceProps={{ size: 10 }}
      />
    );
  }

  protected demo3() {
    const iconStyle: CSSProperties = { cursor: 'pointer', fontSize: 18, fontWeight: 'bold' };
    return (
      <ToolBar
        // height={48}
        style={{ backgroundColor: '#f8f8f8' }}
        title="自定义标题"
        rightContent={[
          <Button key="001" type="primary" icon={<PlusOutlined />}>
            新增
          </Button>,
          <Divider key="002" type="vertical" />,
          <Input.Search key="004" placeholder="输入关键字过滤" style={{ width: 230 }} allowClear={true} />,
          <ColumnHeightOutlined key="003" style={iconStyle} />,
          <FullscreenOutlined key="005" style={iconStyle} />,
          <ReloadOutlined key="006" style={iconStyle} />,
          <SettingOutlined key="007" style={iconStyle} />,
        ]}
        rightSpaceProps={{ size: 10 }}
      />
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="ToolBar(工具栏)">
        <h3>基本使用</h3>
        {this.demo1()}
        <br />
        <br />
        <h3>自定义组件</h3>
        {this.demo2()}
        <br />
        <br />
        <h3>自定义样式</h3>
        {this.demo3()}
        <br />
      </PageWrapper>
    );
  }
}

export default ToolBarDemo;
