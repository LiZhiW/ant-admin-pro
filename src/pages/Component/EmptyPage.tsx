import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { PageWrapper } from '@/components/Layout';

interface EmptyPageProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface EmptyPageState {}

class EmptyPage extends Component<EmptyPageProps, EmptyPageState> {
  render() {
    return <PageWrapper pageHeaderTitle="空白页">空白页</PageWrapper>;
  }
}

export default EmptyPage;
