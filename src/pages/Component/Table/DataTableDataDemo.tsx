import React, { Component } from 'react';
import { WarningOutlined } from '@ant-design/icons';
import { Dispatch } from 'redux';
import { PageWrapper } from '@/components/Layout/PageWrapper';
import { DataTable } from '@/components/Table';
import { DataValueType, DataWrapperType, TableColumn } from '@/components/Table/table-types';

const dataSource = [
  { id: 103, number: 465, money: 36.47, date: '2012-09-25', time: '12:34:11', timeBefore: '2020-06-30 20:30:35', dataSize: 355407, mapper: 4, tag: '第4', progress: 8 },
  { id: 102, number: 464, money: 36.36, date: '2012-09-24', time: '12:34:10', timeBefore: '2020-06-30 19:20:35', dataSize: 155407, mapper: 3, tag: '第3', progress: 7 },
  { id: 101, number: 463, money: 36.25, date: '2012-09-23', time: '12:34:09', timeBefore: '2020-06-30 18:10:35', dataSize: 1407, mapper: 1, tag: '第1', progress: 3 },
  { id: 104, number: 466, money: 36.58, date: '2012-09-26', time: '12:34:12', timeBefore: '2020-06-30 21:40:35', dataSize: 855407, mapper: 2, tag: '第2', progress: 11 },
  { id: 105, number: 467, money: 36.69, date: '2012-09-27', time: '12:34:13', timeBefore: '2020-06-30 22:50:35', dataSize: 1032407, mapper: 3, tag: '第3', progress: 13.5 },
  { id: 106, number: 468, money: 36.8, date: '2012-09-28', time: '12:34:14', timeBefore: '2020-07-01 00:00:35', dataSize: 1308607, mapper: 4, tag: '第4', progress: 16 },
  { id: 107, number: 469, money: 36.91, date: '2012-09-29', time: '12:34:15', timeBefore: '2020-07-01 01:10:35', dataSize: 1584807, mapper: 2, tag: '第2', progress: 18.5 },
  { id: 108, number: 470, money: 37.02, date: '2012-09-30', time: '12:34:16', timeBefore: '2020-07-01 02:20:35', dataSize: 1861007, mapper: 2, tag: '第2', progress: 21 },
  { id: 109, number: 471, money: 37.13, date: '2012-10-01', time: '12:34:17', timeBefore: '2020-07-01 03:30:35', dataSize: 2137207, mapper: 1, tag: '第1', progress: 23.5 },
  { id: 110, number: 472, money: 37.24, date: '2012-10-02', time: '12:34:18', timeBefore: '2020-07-01 04:40:35', dataSize: 2413407, mapper: 3, tag: '第3', progress: 26 },
  { id: 111, number: 473, money: 37.35, date: '2012-10-03', time: '12:34:19', timeBefore: '2020-07-01 05:50:35', dataSize: 2689607, mapper: 2, tag: '第2', progress: 28.5 },
  { id: 112, number: 474, money: 37.46, date: '2012-10-04', time: '12:34:20', timeBefore: '2020-07-01 07:00:35', dataSize: 2965807, mapper: 1, tag: '第1', progress: 31 },
  { id: 113, number: 475, money: 37.57, date: '2012-10-05', time: '12:34:21', timeBefore: '2020-07-01 08:10:35', dataSize: 3242007, mapper: 2, tag: '第2', progress: 33.5 },
  { id: 114, number: 476, money: 37.68, date: '2012-10-06', time: '12:34:22', timeBefore: '2020-07-01 09:20:35', dataSize: 3518207, mapper: 2, tag: '第2', progress: 36 },
  { id: 115, number: 477, money: 37.79, date: '2012-10-07', time: '12:34:23', timeBefore: '2020-07-01 10:30:35', dataSize: 3794407, mapper: 1, tag: '第1', progress: 38.5 },
  { id: 116, number: 478, money: 37.9, date: '2012-10-08', time: '12:34:24', timeBefore: '2020-07-01 11:40:35', dataSize: 4070607, mapper: 1, tag: '第1', progress: 41 },
  { id: 117, number: 479, money: 38.01, date: '2012-10-09', time: '12:34:25', timeBefore: '2020-07-01 12:50:35', dataSize: 4346807, mapper: 3, tag: '第3', progress: 43.5 },
  { id: 118, number: 480, money: 38.12, date: '2012-10-10', time: '12:34:26', timeBefore: '2020-07-01 14:00:35', dataSize: 4623007, mapper: 4, tag: '第4', progress: 46 },
  { id: 119, number: 481, money: 38.23, date: '2012-10-11', time: '12:34:27', timeBefore: '2020-07-01 15:10:35', dataSize: 4899207, mapper: 5, tag: '第5', progress: 48.5 },
  { id: 120, number: 482, money: 38.34, date: '2012-10-12', time: '12:34:28', timeBefore: '2020-07-01 16:20:35', dataSize: 5175407, mapper: 3, tag: '第3', progress: 51 },
  { id: 121, number: 483, money: 38.45, date: '2012-10-13', time: '12:34:29', timeBefore: '2020-07-01 17:30:35', dataSize: 5451607, mapper: 3, tag: '第3', progress: 53.5 },
  { id: 122, number: 484, money: 38.56, date: '2012-10-14', time: '12:34:30', timeBefore: '2020-07-01 18:40:35', dataSize: 5727807, mapper: 2, tag: '第2', progress: 56 },
  { id: 123, number: 485, money: 38.67, date: '2012-10-15', time: '12:34:31', timeBefore: '2020-07-01 19:50:35', dataSize: 6004007, mapper: 1, tag: '第1', progress: 58.5 },
  { id: 124, number: 486, money: 38.78, date: '2012-10-16', time: '12:34:32', timeBefore: '2020-07-01 21:00:35', dataSize: 6280207, mapper: 2, tag: '第2', progress: 61 },
  { id: 125, number: 487, money: 38.89, date: '2012-10-17', time: '12:34:33', timeBefore: '2020-07-01 22:10:35', dataSize: 6556407, mapper: 1, tag: '第1', progress: 63.5 },
  { id: 126, number: 488, money: 39.0, date: '2012-10-18', time: '12:34:34', timeBefore: '2020-07-01 23:20:35', dataSize: 6832607, mapper: 1, tag: '第1', progress: 66 },
  { id: 127, number: 489, money: 39.11, date: '2012-10-19', time: '12:34:35', timeBefore: '2020-07-02 00:30:35', dataSize: 7108807, mapper: 3, tag: '第3', progress: 68.5 },
  { id: 128, number: 490, money: 39.22, date: '2012-10-20', time: '12:34:36', timeBefore: '2020-07-02 01:40:35', dataSize: 7385007, mapper: 4, tag: '第4', progress: 71 },
  { id: 129, number: 491, money: 39.33, date: '2012-10-21', time: '12:34:37', timeBefore: '2020-07-02 02:50:35', dataSize: 7661207, mapper: 4, tag: '第4', progress: 73.5 },
  { id: 130, number: 492, money: 39.44, date: '2012-10-22', time: '12:34:38', timeBefore: '2020-07-02 04:00:35', dataSize: 7937407, mapper: 2, tag: '第2', progress: 76 },
  { id: 131, number: 493, money: 39.55, date: '2012-10-23', time: '12:34:39', timeBefore: '2020-07-02 05:10:35', dataSize: 8213607, mapper: 1, tag: '第1', progress: 78.5 },
  { id: 132, number: 494, money: 39.66, date: '2012-10-24', time: '12:34:40', timeBefore: '2020-07-02 06:20:35', dataSize: 8489807, mapper: 2, tag: '第2', progress: 81 },
  { id: 133, number: 495, money: 39.77, date: '2012-10-25', time: '12:34:41', timeBefore: '2020-07-02 07:30:35', dataSize: 8766007, mapper: 3, tag: '第3', progress: 83.5 },
  { id: 134, number: 496, money: 39.88, date: '2012-10-26', time: '12:34:42', timeBefore: '2020-07-02 08:40:35', dataSize: 9042207, mapper: 2, tag: '第2', progress: 86 },
  { id: 135, number: 497, money: 39.99, date: '2012-10-27', time: '12:34:43', timeBefore: '2020-07-02 09:50:35', dataSize: 9318407, mapper: 3, tag: '第3', progress: 88.5 },
  { id: 136, number: 498, money: 40.1, date: '2012-10-28', time: '12:34:44', timeBefore: '2020-07-02 11:00:35', dataSize: 9594607, mapper: 4, tag: '第4', progress: 91 },
  { id: 137, number: 499, money: 40.21, date: '2012-10-29', time: '12:34:45', timeBefore: '2020-07-02 12:10:35', dataSize: 9870807, mapper: 2, tag: '第2', progress: 93.5 },
  { id: 138, number: 500, money: 40.32, date: '2012-10-30', time: '12:34:46', timeBefore: '2020-07-02 13:20:35', dataSize: 10147007, mapper: 2, tag: '第2', progress: 96 },
  { id: 139, number: 501, money: 40.43, date: '2012-10-31', time: '12:34:47', timeBefore: '2020-07-02 14:30:35', dataSize: 10423207, mapper: 4, tag: '第4', progress: 98.5 },
  { id: 140, number: 502, money: 40.54, date: '2012-11-01', time: '12:34:48', timeBefore: '2020-07-02 15:40:35', dataSize: 10699407, mapper: 2, tag: '第2', progress: 56 },
  { id: 141, number: 503, money: 40.65, date: '2012-11-02', time: '12:34:49', timeBefore: '2020-07-02 16:50:35', dataSize: 10975607, mapper: 1, tag: '第1', progress: 74 },
  { id: 142, number: 504, money: 40.76, date: '2012-11-03', time: '12:34:50', timeBefore: '2020-07-02 18:00:35', dataSize: 11251807, mapper: 2, tag: '第2', progress: 36 },
  { id: 143, number: 505, money: 40.87, date: '2012-11-04', time: '12:34:51', timeBefore: '2020-07-02 19:10:35', dataSize: 11528007, mapper: 3, tag: '第3', progress: 95 },
  { id: 144, number: 506, money: 40.98, date: '2012-11-05', time: '12:34:52', timeBefore: '2020-07-02 20:20:35', dataSize: 11804207, mapper: 2, tag: '第2', progress: 24 },
  { id: 145, number: 507, money: 41.09, date: '2012-11-06', time: '12:34:53', timeBefore: '2020-07-02 21:30:35', dataSize: 12080407, mapper: 3, tag: '第3', progress: 67 },
  { id: 146, number: 508, money: 41.2, date: '2012-11-07', time: '12:34:54', timeBefore: '2020-07-02 22:40:35', dataSize: 12356607, mapper: 2, tag: '第2', progress: 18 },
];

const mapperData = [
  { value: 1, label: '状态1' },
  { value: 2, label: '状态2' },
  { value: 3, label: '状态3', style: { color: 'red' } },
  { value: 4, label: '状态4', customLabel: <WarningOutlined /> },
];

const columnsA: TableColumn[] = [
  // {dataIndex: "id", title: "#ID", width: 60, sorter: true},
  { dataIndex: 'storeNo', title: '店铺编号', ellipsis: true, sorter: true },
  { dataIndex: 'orderCode', title: '订单编码', ellipsis: true, sorter: true },
  // {dataIndex: "storeProdNo", title: "店铺商品编码", ellipsis: true, sorter: true},
  // {dataIndex: "erpNo", title: "ERP编码", ellipsis: true, sorter: true},
  { dataIndex: 'prodName', title: '商品名称', ellipsis: true, sorter: true },
  { dataIndex: 'prodSpecification', title: '规格', ellipsis: true, sorter: true, width: 120 },
  { dataIndex: 'packageUnit', title: '单位', ellipsis: true, sorter: true, width: 60 },
  { dataIndex: 'manufacture', title: '厂家', ellipsis: true, sorter: true },
  { dataIndex: 'outNumber', title: '出库数量', ellipsis: true, sorter: true, width: 90, suffix: (originalValue, value, record) => record?.packageUnit ?? '' },
  {
    dataIndex: 'goodsStatus',
    title: '状态',
    ellipsis: true,
    sorter: true,
    wrapperType: DataWrapperType.Mapper,
    width: 80,
    mapperData: [
      { value: '0', label: '未出库' },
      { value: '1', label: '已出库', style: { color: '#52c41a' } },
      { value: '2', label: '不出库', style: { color: '#f5222d' } },
      { value: '3', label: '部分出库', style: { color: '#faad14' } },
    ],
  },
  { dataIndex: 'memberPrice', title: '会员价', ellipsis: true, sorter: true, valueType: DataValueType.Money, width: 110 },
  // {dataIndex: "storeDiscount", title: "店铺优惠", ellipsis: true, sorter: true, valueType: DataValueType.Money},
  // {dataIndex: "platformDiscount", title: "平台优惠", ellipsis: true, sorter: true, valueType: DataValueType.Money},
  { dataIndex: 'createAt', title: '创建时间', ellipsis: true, sorter: true, sortOrder: 'descend', valueType: DataValueType.Date, width: 100 },
];

const columnsB: TableColumn[] = [
  { dataIndex: 'id', title: '#ID', width: 60, sorter: true, sortOrder: 'descend' },
  { dataIndex: 'number', title: '数字', valueType: DataValueType.Number, sorter: true },
  { dataIndex: 'money', title: '金额', valueType: DataValueType.Money, prefix: '￥', sorter: true },
  { dataIndex: 'date', title: '日期', valueType: DataValueType.Date, sorter: true },
  { dataIndex: 'time', title: '时间', valueType: DataValueType.Time, sorter: true },
  { dataIndex: 'timeBefore', title: '相对时间', valueType: DataValueType.TimeBefore, sorter: true },
  { dataIndex: 'dataSize', title: '大小', valueType: DataValueType.DataBytesSize, sorter: true },
  { dataIndex: 'mapper', title: '状态', wrapperType: DataWrapperType.Mapper, mapperData, sorter: true },
  { dataIndex: 'tag', title: '标签', wrapperType: DataWrapperType.Tag, sorter: true },
  { dataIndex: 'progress', title: '进度条', valueType: DataValueType.Number, wrapperType: DataWrapperType.Progress, sorter: true },
];

interface DataTableDemoProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface DataTableDemoState {}

class DataTableDataDemo extends Component<DataTableDemoProps, DataTableDemoState> {
  demo3Ref: DataTable | null = null;

  demo5Ref: DataTable | null = null;

  // 服务端数据(分页+排序+筛选)
  protected demo1() {
    return (
      <DataTable
        // indexColumn={false}
        columns={columnsA}
        columnActionWidth={120}
        maxActionItem={3}
        columnActions={[
          { actionText: '详情' },
          { actionText: '编辑' },
          {
            actionText: '删除',
            actionStyle: { color: '#f5222d' },
            onAction: (record) => {
              // eslint-disable-next-line no-console
              console.log('record -> ', record);
            },
          },
        ]}
        rowKey="id"
        initPagination={true}
        // initPagination={{current: 2}}
        serverPaginationData={{
          url: '/api/query_page/find3',
        }}
      />
    );
  }

  // 客户端数据(分页+排序+筛选)
  protected demo2() {
    return (
      <DataTable
        // indexColumn={false}
        columns={columnsB}
        columnActionWidth={120}
        maxActionItem={3}
        columnActions={[
          { actionText: '详情' },
          { actionText: '编辑' },
          {
            actionText: '删除',
            actionStyle: { color: '#f5222d' },
            onAction: (record) => {
              // eslint-disable-next-line no-console
              console.log('record -> ', record);
            },
          },
        ]}
        rowKey="id"
        initPagination={true}
        initLocalDataSource={dataSource}
      />
    );
  }

  // 服务端数据(客户端分页+客户端排序+客户端筛选)
  protected demo3() {
    // noinspection DuplicatedCode
    return (
      <DataTable
        ref={(demo3) => {
          this.demo3Ref = demo3;
        }}
        // indexColumn={false}
        columns={columnsA}
        columnActionWidth={120}
        maxActionItem={3}
        columnActions={[
          { actionText: '详情' },
          { actionText: '编辑' },
          {
            actionText: '删除',
            actionStyle: { color: '#f5222d' },
            onAction: (record) => {
              // eslint-disable-next-line no-console
              console.log('record -> ', record);
            },
          },
        ]}
        rowKey="id"
        initPagination={true}
        serverData={{
          url: '/api/query_page/find4',
        }}
      />
    );
  }

  // 数据筛选
  protected demo4() {}

  // 数据选择
  protected demo5() {
    return (
      <DataTable
        ref={(demo5) => {
          this.demo5Ref = demo5;
        }}
        // indexColumn={false}
        selectionMode={'checkbox'}
        columns={columnsB}
        columnActionWidth={120}
        maxActionItem={3}
        columnActions={[{ actionText: '详情' }, { actionText: '编辑' }, { actionText: '删除', actionStyle: { color: '#f5222d' } }]}
        rowKey="id"
        initPagination={true}
        initLocalDataSource={dataSource}
      />
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="数据表格">
        <h3>服务端数据(分页+排序+筛选)</h3>
        {this.demo1()}
        <br />
        <br />
        <h3>客户端数据(分页+排序+筛选)</h3>
        {this.demo2()}
        <br />
        <br />
        <h3>
          服务端数据(客户端分页+客户端排序+客户端筛选) <a onClick={() => this.demo3Ref?.reloadServerData()}>刷新</a>
        </h3>
        {this.demo3()}
        <br />
        <br />
        <h3>数据筛选</h3>
        {this.demo4()}
        <br />
        <br />
        <h3>
          数据选择
          <a style={{ marginLeft: 8 }} onClick={() => this.demo5Ref?.setSelectedRowKeys([106, 107, 108, 146])}>
            设置
          </a>
          {/* eslint-disable-next-line */}
          <a style={{ marginLeft: 8 }} onClick={() => console.log(this.demo5Ref?.getSelectedRows())}>
            获取
          </a>
        </h3>
        {this.demo5()}
        <br />
        <br />
      </PageWrapper>
    );
  }
}

export default DataTableDataDemo;

// 数据分页(客户端 + 服务端OK)
// 多列排序(客户端 + 服务端)
// 数据筛选(客户端 + 服务端?)
// 数据选择(单选 + 多选)
// 数据导出(Excel + CSV + ?)
// 行展开?
