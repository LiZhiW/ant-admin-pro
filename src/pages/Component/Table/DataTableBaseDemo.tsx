import React, { Component } from 'react';
import { message } from 'antd';
import { DeleteOutlined, InfoCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { Dispatch } from 'redux';
import { PageWrapper } from '@/components/Layout';
import { ActionType, DataValueType, DataWrapperType } from '@/components/Table/table-types';
import { DataTable } from '@/components/Table';

interface DataTableDemoProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface DataTableDemoState {}

class DataTableBaseDemo extends Component<DataTableDemoProps, DataTableDemoState> {
  // 基本使用(DataValueType)
  protected demo1() {
    return (
      <DataTable
        // indexColumn={false}
        columns={[
          { dataIndex: 'a', title: 'Order', order: 1 },
          { dataIndex: 'b', title: '前缀、后缀', prefix: 'p-', suffix: '-s' },
          { dataIndex: 'c', title: '数字', valueType: DataValueType.Number },
          { dataIndex: 'd', title: '金额', valueType: DataValueType.Money, prefix: '￥' },
          { dataIndex: 'e', title: '日期', valueType: DataValueType.Date },
          { dataIndex: 'f', title: '日期时间', valueType: DataValueType.DateTime },
          { dataIndex: 'g', title: '时间', valueType: DataValueType.Time },
          { dataIndex: 'h', title: '日期(时间)范围', valueType: DataValueType.DateRange },
          { dataIndex: 'i', title: 'TimeBefore', valueType: DataValueType.TimeBefore },
          { dataIndex: 'i', title: 'HowLongTime', valueType: DataValueType.HowLongTime, key: 'i_2' },
          { dataIndex: 'j', title: '数据大小', valueType: DataValueType.DataBytesSize },
          { dataIndex: 'k', title: '自定义render', render: (val) => <span style={{ color: 'red' }}>{val}</span> },
        ]}
        initLocalDataSource={[
          { a: 'a1', b: 'b', c: 0, d: '1', e: new Date(), f: new Date(), g: new Date(), h: [new Date(), '2012-09-23'], i: '155407', j: -127234, k: 1 },
          { a: 'a2', b: 'b', c: '2.2', d: 22323232, e: '2012-09-23', f: '2012-09-23', g: '2012-09-23', h: [1589508155407, '2012-09-23 12:34:09'], i: -1955407, j: '12723', k: 2 },
          {
            a: 'a3',
            b: 'b',
            c: '3.22',
            d: -343.34,
            e: '2012-09-23 12:34:09',
            f: '2012-09-23 12:34:09',
            g: '2012-09-23 12:34:09',
            h: [null, 1589508155407],
            i: null,
            j: '78678',
            k: 3,
          },
          {
            a: 'a4',
            b: 'b',
            c: '4.2112',
            d: '4.3',
            e: 1589508155407,
            f: 1589508155407,
            g: 1589508155407,
            h: '2012-09-23 12:34:09',
            i: '2020-05-13 12:34:09',
            j: '7867878678',
            k: 0,
          },
          {
            a: 'a5',
            b: 'b',
            c: '51212.3',
            d: '535353434.3434',
            e: '2019-09-23 12:34:09',
            f: '2019-09-23 12:34:09',
            g: '2019-09-23 12:34:09',
            h: [],
            i: new Date(),
            j: 12723,
            k: null,
          },
        ]}
        rowKey="a"
      />
    );
  }

  // 基本使用(DataWrapperType)
  protected demo2() {
    const dataSource = [
      { a: 'a1', b: '1', c: 'lzw', d: '0', e: '0', f: '0', g: '0', h: '0', i: '0', j: '0', k: '0', l: '0', m: ['0', '1', '2'], n: '0', o: '0', p: '0', q: '0', r: '0', s: '0' },
      {
        a: 'a2',
        b: '2',
        c: 'xzs',
        d: '1',
        e: '1',
        f: '1',
        g: '1',
        h: '10',
        i: '1',
        j: '1',
        k: '1',
        l: '1',
        m: [
          { value: '1', label: '第一' },
          { value: '2', label: '第二' },
        ],
        n: '1',
        o: '1',
        p: '1',
        q: '1',
        r: '1',
        s: '1',
      },
      { a: 'a3', b: '3', c: 'sdf', d: '2', e: '2', f: '2', g: '2', h: 29, i: '2', j: '2', k: '2', l: '2', m: '2', n: '2', o: '2', p: undefined, q: '2', r: '2', s: '2' },
      { a: 'a4', b: '4', c: 'gtt', d: '3', e: '3', f: '3', g: '3', h: '98', i: '3', j: '3', k: '3', l: '3', m: '3', n: '3', o: '3', p: ['0', '1', '2'], q: '3', r: '3', s: '3' },
      {
        a: 'a5',
        b: '5',
        c: 'pfu',
        d: '4',
        e: '4',
        f: '4',
        g: '4',
        h: -45,
        i: '4',
        j: '4',
        k: '4',
        l: '4',
        m: '4',
        n: '4',
        o: '4',
        p: [
          { value: '1', label: '第一' },
          { value: '2', label: '第二' },
        ],
        q: '4',
        r: '4',
        s: '4',
      },
    ];
    return (
      <>
        <DataTable
          // indexColumn={false}
          columns={[
            { dataIndex: 'a', title: 'Order', order: 1 },
            {
              dataIndex: 'b',
              title: 'Mapper',
              wrapperType: DataWrapperType.Mapper,
              mapperData: [
                { value: 1, label: '状态1' },
                { value: 2, label: '状态2' },
                { value: 3, label: '状态3', style: { color: 'red' } },
                { value: 4, label: '状态4', customLabel: <WarningOutlined /> },
              ],
            },
            {
              dataIndex: 'c',
              title: 'Avatar',
              wrapperType: DataWrapperType.Avatar,
              // getAvatarProps: [{value: 'lzw', shape: "square", style: {backgroundColor: "red"}}],
              getAvatarProps: (originalValue: any, value: any) => {
                const src = originalValue === 'lzw' ? 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' : undefined;
                return { style: { backgroundColor: '#f56a00' }, size: 'small', src, children: (value as string).toUpperCase() };
              },
            },
            {
              dataIndex: 'd',
              title: 'Badge',
              wrapperType: DataWrapperType.Badge,
              // getBadgeProps: [{value: 3, status: "processing", text: "进行中..."}],
              getBadgeProps: (originalValue: any) => {
                return { status: originalValue === '3' ? 'processing' : 'error', text: originalValue === '1' ? '错误' : originalValue };
              },
            },
            {
              dataIndex: 'e',
              title: 'Popover',
              wrapperType: DataWrapperType.Popover,
              // getPopoverProps: [{value: "0", children: <a>详情1</a>, content: "自定义详情", title: "标题"}],
              getPopoverProps: (originalValue: any) => {
                return { content: originalValue === '1' ? '自定义详情' : originalValue };
              },
            },
            {
              dataIndex: 'f',
              title: 'Tooltip',
              wrapperType: DataWrapperType.Tooltip,
              // getTooltipProps: [{value: 1, title: "啦啦啦", children: <span>查看</span>}],
              getTooltipProps: (originalValue: any) => {
                return { children: <span>查看</span>, title: originalValue === '0' ? '呱呱呱' : originalValue };
              },
            },
            {
              dataIndex: 'g',
              title: 'Tag',
              wrapperType: DataWrapperType.Tag,
              // getTagProps: [{value: "0", color: "red"}, {value: 3, color: "blue", children: "成功"}],
              getTagProps: (originalValue: any) => {
                return { color: 'success', children: originalValue === '1' ? ['No.1', 'No.2'] : originalValue };
              },
            },
            {
              dataIndex: 'h',
              title: 'Progress',
              valueType: DataValueType.Number,
              wrapperType: DataWrapperType.Progress,
              // getProgressProps: [{value: -45, percent: 45, status: "exception"}, {value: 98, status: "active"}],
              getProgressProps: (originalValue: any, value: any) => {
                return { status: value >= 29 ? 'active' : 'normal' };
              },
            },
            {
              dataIndex: 'i',
              title: 'PopConfirm',
              wrapperType: DataWrapperType.PopConfirm,
              getPopConfirmProps: [
                { value: '0', children: <a>新增</a>, title: '确认新增?' },
                { value: 4, children: <a>删除</a>, title: '确认删除?', onConfirm: () => message.success('删除成功') },
              ],
              // getPopConfirmProps: (originalValue: any) => {
              //   return {children: <a>{originalValue === "0" ? "删除" : "新增"}</a>, title: originalValue === "0" ? "确认删除?" : "确认新增?"};
              // },
            },
            {
              dataIndex: 'j',
              title: 'Link',
              wrapperType: DataWrapperType.Link,
              getLinkProps: [{ value: '1', children: '链接1', to: '/component/data_display/detail/detail_table' }],
            },
            {
              dataIndex: 'k',
              title: 'ModalDialog',
              wrapperType: DataWrapperType.ModalDialog,
              getModalDialogProps: [{ value: 2, method: 'error', children: '错误', content: '异常信息', okText: '重试', okButtonProps: { type: 'ghost' } }],
            },
            { dataIndex: 's', title: '自定义render', render: (val) => <span style={{ color: 'red' }}>{val}</span> },
          ]}
          initLocalDataSource={dataSource}
          rowKey="a"
        />
        <br />
        <DataTable
          columns={[
            {
              dataIndex: 'l',
              title: 'Checkbox',
              wrapperType: DataWrapperType.Checkbox,
              getCheckboxProps: [{ value: '3', children: '第三', label: false }],
            },
            {
              dataIndex: 'm',
              title: 'CheckboxGroup',
              wrapperType: DataWrapperType.CheckboxGroup,
              // getCheckboxGroupProps: [
              //   {value: 3, options: ["1", "2", "3"], defaultValue: ["2", "3"]},
              //   {value: 4, options: [{value: 1, label: "第一"}, {value: 2, label: "第二"}, {value: 3, label: "第三"}], defaultValue: [1, 3]},
              // ],
              getCheckboxGroupProps: (originalValue) => {
                return {
                  options: [
                    { value: 1, label: '选项一' },
                    { value: 2, label: '选项二' },
                    { value: 3, label: '选项三' },
                  ],
                  defaultValue: [originalValue === '2' ? 2 : originalValue === '3' ? 3 : 1],
                };
              },
            },
            {
              dataIndex: 'n',
              title: 'Rate',
              wrapperType: DataWrapperType.Rate,
              getRateProps: [{ value: 0, label: 0.5, allowHalf: true }],
            },
            {
              dataIndex: 'o',
              title: 'Radio',
              wrapperType: DataWrapperType.Radio,
              getRadioProps: [{ value: '1', children: '第一', label: false }],
            },
            {
              dataIndex: 'p',
              title: 'RadioGroup',
              wrapperType: DataWrapperType.RadioGroup,
              getRadioGroupProps: [
                { value: 0, options: ['1', '2', '3'], defaultValue: '3' },
                {
                  value: 1,
                  options: [
                    { value: 1, label: '一' },
                    { value: 2, label: '二' },
                    { value: 3, label: '三' },
                  ],
                  defaultValue: 2,
                },
              ],
              // getRadioGroupProps: originalValue => {
              //   return {
              //     options: [{value: 2, label: "二"}, {value: 3, label: "三"}, {value: 4, label: "四"}],
              //     defaultValue: originalValue === "0" ? 4 : originalValue === "1" ? 3 : 2,
              //   };
              // },
            },
            {
              dataIndex: 'q',
              title: 'Switch',
              wrapperType: DataWrapperType.Switch,
              getSwitchProps: [{ value: 4, checkedChildren: '开', unCheckedChildren: '关', checked: false }],
            },
            {
              dataIndex: 'r',
              title: 'Slider',
              wrapperType: DataWrapperType.Slider,
              getSliderProps: [{ value: 2, label: 80 }],
            },
          ]}
          initLocalDataSource={dataSource}
          rowKey="a"
        />
      </>
    );
  }

  // 基本使用(TableColumnAction)
  protected demo3() {
    return (
      <DataTable
        // indexColumn={false}
        columns={[
          { dataIndex: 'a', title: '列A' },
          { dataIndex: 'b', title: '列B' },
          { dataIndex: 'c', title: '列C' },
          { dataIndex: 'd', title: '列D' },
          { dataIndex: 'e', title: '列E' },
          { dataIndex: 'f', title: '列F' },
          { dataIndex: 'g', title: '列G' },
        ]}
        columnActions={[
          { actionText: '详情', actionIcon: <InfoCircleOutlined style={{ fontSize: 12 }} />, actionStyle: { color: 'green' } },
          { actionText: '编辑', actionType: ActionType.PopConfirm, appendDivider: true },
          { actionText: '禁用', actionStyle: { color: 'orange' } },
          { actionText: '删除', actionIcon: <DeleteOutlined />, actionStyle: { color: 'red' }, appendDivider: true },
          { actionText: '成功', actionType: ActionType.ModalDialog, getModalDialogProps: { method: 'success' }, order: 1 },
          { actionText: '错误', actionType: ActionType.ModalDialog, getModalDialogProps: { method: 'error' }, order: 1 },
          { actionText: '提示', actionType: ActionType.ModalDialog, getModalDialogProps: { method: 'info' }, order: 1 },
          { actionText: '警告', actionType: ActionType.ModalDialog, getModalDialogProps: { method: 'warning' } },
          { actionText: '确认', actionType: ActionType.ModalDialog, getModalDialogProps: { method: 'confirm' }, appendDivider: true },
          { actionText: '很长很长很长很长很长', actionType: ActionType.Link, getLinkProps: { to: '/component/data_display/detail/detail_modal' }, appendDivider: true },
        ]}
        // moreActionButton={<a style={{whiteSpace: 'nowrap'}}>更多<DownOutlined/></a>}
        // columnActionWidth={160}
        initLocalDataSource={[
          { a: 'a1', b: 'b1', c: 'c1', d: 'd1', e: 'e1', f: 'f1', g: 'g1', h: 'h1', i: 'i1', j: 'j1', k: 'k1' },
          { a: 'a2', b: 'b2', c: 'c2', d: 'd2', e: 'e2', f: 'f2', g: 'g2', h: 'h2', i: 'i2', j: 'j2', k: 'k2' },
          { a: 'a3', b: 'b3', c: 'c3', d: 'd3', e: 'e3', f: 'f3', g: 'g3', h: 'h3', i: 'i3', j: 'j3', k: 'k3' },
          { a: 'a4', b: 'b4', c: 'c4', d: 'd4', e: 'e4', f: 'f4', g: 'g4', h: 'h4', i: 'i4', j: 'j4', k: 'k4' },
          { a: 'a5', b: 'b5', c: 'c5', d: 'd5', e: 'e5', f: 'f5', g: 'g5', h: 'h5', i: 'i5', j: 'j5', k: 'k5' },
        ]}
        rowKey="a"
      />
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="数据表格">
        <h3>基本使用(DataValueType)</h3>
        {this.demo1()}
        <br />
        <br />
        <h3>基本使用(DataWrapperType)</h3>
        {this.demo2()}
        <br />
        <br />
        <h3>基本使用(TableColumnAction)</h3>
        {this.demo3()}
        <br />
        <br />
      </PageWrapper>
    );
  }
}

export default DataTableBaseDemo;
