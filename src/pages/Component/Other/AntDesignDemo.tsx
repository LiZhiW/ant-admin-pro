import React, { Component } from 'react';
import { Button, Form, Input, InputNumber, Select, Table, Tag } from 'antd';
import { Dispatch } from 'redux';
import { PageWrapper } from '@/components/Layout';

interface AntDesignProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface AntDesignState {}

class AntDesignDemo extends Component<AntDesignProps, AntDesignState> {
  protected select() {
    return (
      <>
        <Select style={{ width: 120 }} size="large" defaultValue="jack">
          <Select.Option value="jack">Jack</Select.Option>
          <Select.Option value="lucy">Lucy</Select.Option>
          <Select.Option value="tom">Tom</Select.Option>
        </Select>
        <span style={{ display: 'inline-block', width: 16 }} />
        <Select style={{ width: 120 }} size="middle" defaultValue="lucy">
          <Select.Option value="jack">Jack</Select.Option>
          <Select.Option value="lucy">Lucy</Select.Option>
          <Select.Option value="tom">Tom</Select.Option>
        </Select>
        <span style={{ display: 'inline-block', width: 16 }} />
        <Select style={{ width: 120 }} size="small" defaultValue="tom">
          <Select.Option value="jack">Jack</Select.Option>
          <Select.Option value="lucy">Lucy</Select.Option>
          <Select.Option value="tom">Tom</Select.Option>
        </Select>
      </>
    );
  }

  protected form() {
    const layout = {
      labelCol: { flex: '120px' },
      wrapperCol: { flex: 'auto' },
    };
    return (
      <Form {...layout} name="test" size="middle" style={{ width: 800 }}>
        <Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name={['user', 'email']} label="Email" rules={[{ type: 'email' }]}>
          <Input />
        </Form.Item>
        <Form.Item name={['user', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 99 }]}>
          <InputNumber />
        </Form.Item>
        <Form.Item name={['user', 'website']} label="Website">
          <Input />
        </Form.Item>
        <Form.Item name={['user', 'introduction']} label="Introduction">
          <Input.TextArea />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
          <Button type="primary" htmlType="submit">
            提交
          </Button>
        </Form.Item>
      </Form>
    );
  }

  protected table() {
    const dataSource: any[] = [];
    dataSource.push({ key: '1', name: 'John Brown', age: 32, address: 'New York No. 1 Lake Park', tags: ['nice', 'developer'] });
    dataSource.push({ key: '2', name: 'Jim Green', age: 42, address: 'London No. 1 Lake Park', tags: ['loser'] });
    dataSource.push({ key: '3', name: 'Joe Black', age: 32, address: 'Sidney No. 1 Lake Park', tags: ['cool', 'teacher'] });
    dataSource.push();
    for (let i = 0; i < 1000; i++) {
      dataSource.push({ key: `g-${i}`, name: `Li ${i}`, age: 32, address: `New York No. ${i} Lake Park`, tags: [`t-${i}`] });
    }
    return (
      <Table
        bordered={true}
        size="middle"
        columns={[
          { title: 'Name', dataIndex: 'name', key: 'name', render: (text) => <a>{text}</a> },
          { title: 'Age', dataIndex: 'age', key: 'age' },
          { title: 'Address', dataIndex: 'address', key: 'address' },
          {
            title: 'Tags',
            key: 'tags',
            dataIndex: 'tags',
            render: (tags) => (
              <span>
                {(tags as string[]).map((tag) => {
                  let color = tag.length > 5 ? 'geekblue' : 'green';
                  if (tag === 'loser') {
                    color = 'volcano';
                  }
                  return (
                    <Tag color={color} key={tag}>
                      {tag.toUpperCase()}
                    </Tag>
                  );
                })}
              </span>
            ),
          },
          {
            title: 'Action',
            key: 'action',
            render: (_, record) => (
              <span>
                <a style={{ marginRight: 16 }}>Invite {record.name}</a>
                <a>Delete</a>
              </span>
            ),
          },
        ]}
        dataSource={dataSource}
        pagination={{ size: 'default', showQuickJumper: true }}
      />
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="详情表格">
        <h3>下拉选择</h3>
        {this.select()}
        <br />
        <br />
        <h3>表单</h3>
        {this.form()}
        <br />
        <br />
        <h3>数据表格</h3>
        {this.table()}
        <br />
      </PageWrapper>
    );
  }
}

export default AntDesignDemo;
