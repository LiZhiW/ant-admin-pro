import React, { Component } from 'react';
import { Col, Row } from 'antd';
import { InfoCircleOutlined, LoadingOutlined, ReloadOutlined } from '@ant-design/icons';
import { Dispatch } from 'redux';
import { PageWrapper } from '@/components/Layout';
import { DetailTable } from '@/components/Detail';
import styles from './DetailTableDemo.less';

interface DetailTableProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface DetailTableState {
  loading_demo4: boolean;
}

class DetailTableDemo extends Component<DetailTableProps, DetailTableState> {
  protected detailTableRef: DetailTable | null = null;

  public state: DetailTableState = {
    loading_demo4: false,
  };

  // 基本使用
  protected demo1() {
    return (
      <DetailTable
        style={{ width: 800 }}
        header="表格标题"
        footer="表格尾部"
        enableEllipsis={true}
        columnCount={3}
        labelSpan={{ type: 'px', width: 150 }}
        borderColor="#e8e8e8"
        labelSuffix=":"
        labels={{
          name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
          deptName: '所属部门',
          dutyType: '班次类型',
          startTime: { label: '上班时间', style: { color: 'red' } },
          endTime: '下班时间',
          onRange: '上班打卡时间范围',
          offRange: '下班打卡时间范围',
          duration: (
            <span>
              <InfoCircleOutlined /> 午休时间
            </span>
          ),
          stats: '状态',
        }}
        dataTransforms={{
          deptName: { transform: (value) => `${value} | ${value}`, columnCount: 2, ellipsis: true },
          stats: [
            { value: 1, label: '成功' },
            { value: 0, label: '失败' },
          ],
        }}
        initData={{
          name: 'lizw',
          deptName: '技术研发部',
          dutyType: '早班',
          startTime: '8:00',
          endTime: '5:00',
          onRange: '8:00-8:30',
          offRange: '5:00-6:00',
          duration: '半小时',
          stats: 0,
        }}
      />
    );
  }

  // 加载远程数据
  protected demo2() {
    return (
      <DetailTable
        style={{ width: 800 }}
        header="表格标题"
        footer="表格尾部"
        enableEllipsis={true}
        columnCount={3}
        labelSpan={{ type: 'px', width: 150 }}
        borderColor="#e8e8e8"
        labels={{
          name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
          deptName: '所属部门',
          dutyType: '班次类型',
          startTime: { label: '上班时间', style: { color: 'red' } },
          endTime: '下班时间',
          onRange: '上班打卡时间范围',
          offRange: '下班打卡时间范围',
          duration: (
            <span>
              <InfoCircleOutlined /> 午休时间
            </span>
          ),
          stats: '状态',
        }}
        dataTransforms={{
          deptName: { transform: (value) => `${value}|${value}`, columnCount: 2, ellipsis: true },
          stats: [
            { value: 1, label: '成功' },
            { value: 0, label: '失败' },
          ],
        }}
        serverData={{
          initLoadData: true,
          url: '/api/query_page/detail_data',
        }}
      />
    );
  }

  // 响应式断点
  protected demo3() {
    return (
      <DetailTable
        enableEllipsis={true}
        columnCount={{
          sm: 1,
          md: 2,
          lg: 3,
          // xl: 4,
          xxl: 5,
        }}
        labelSpan={{ type: 'px', width: 150 }}
        borderColor="#e8e8e8"
        labelSuffix=":"
        labels={{
          name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
          deptName: '所属部门',
          dutyType: '班次类型',
          startTime: { label: '上班时间', style: { color: 'red' } },
          endTime: '下班时间',
          onRange: '上班打卡时间范围',
          offRange: '下班打卡时间范围',
          duration: (
            <span>
              <InfoCircleOutlined /> 午休时间
            </span>
          ),
          stats: '状态',
        }}
        dataTransforms={{
          deptName: { transform: (value) => `${value} | ${value}`, columnCount: 2, ellipsis: true },
          stats: [
            { value: 1, label: '成功' },
            { value: 0, label: '失败' },
          ],
        }}
        initData={{
          name: 'lizw',
          deptName: '技术研发部',
          dutyType: '早班',
          startTime: '8:00',
          endTime: '5:00',
          onRange: '8:00-8:30',
          offRange: '5:00-6:00',
          duration: '半小时',
          stats: 0,
        }}
      />
    );
  }

  // 调用组件方法
  protected demo4() {
    const { loading_demo4 } = this.state;
    return (
      <DetailTable
        ref={(ref) => {
          this.detailTableRef = ref;
        }}
        onLoadingChange={(loading) => this.setState({ loading_demo4: loading })}
        style={{ width: 800 }}
        header={
          <Row>
            <Col flex="auto">数据详情</Col>
            <Col flex="16px">
              {loading_demo4 ? (
                <LoadingOutlined className={styles.icon} />
              ) : (
                <ReloadOutlined
                  className={styles.reload}
                  onClick={async () => {
                    if (this.detailTableRef) {
                      await this.detailTableRef.refresh();
                    }
                  }}
                />
              )}
            </Col>
          </Row>
        }
        enableEllipsis={true}
        columnCount={3}
        labelSpan={{ type: 'px', width: 150 }}
        borderColor="#d8ecfc"
        labelBackgroundColor="#e7f1fa"
        labels={{
          name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
          deptName: '所属部门',
          dutyType: '班次类型',
          startTime: { label: '上班时间', style: { color: 'red' } },
          endTime: '下班时间',
          onRange: '上班打卡时间范围',
          offRange: '下班打卡时间范围',
          duration: (
            <span>
              <InfoCircleOutlined /> 午休时间
            </span>
          ),
          stats: '状态',
        }}
        dataTransforms={{
          deptName: { transform: (value) => `${value}|${value}`, columnCount: 2, ellipsis: true },
          stats: [
            { value: 1, label: '成功' },
            { value: 0, label: '失败' },
          ],
        }}
        initData={{
          name: 'lizw',
          deptName: '技术研发部',
          dutyType: '早班',
          startTime: '8:00',
          endTime: '5:00',
          onRange: '8:00-8:30',
          offRange: '5:00-6:00',
          duration: '半小时',
          stats: 0,
        }}
        serverData={{
          initLoadData: true,
          url: '/api/query_page/detail_data',
        }}
      />
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="详情表格">
        <h3>基本使用</h3>
        {this.demo1()}
        <br />
        <br />
        <h3>加载远程数据</h3>
        {this.demo2()}
        <br />
        <br />
        <h3>响应式断点-尝试调整浏览器窗口宽度</h3>
        {this.demo3()}
        <br />
        <br />
        <h3>调用组件方法</h3>
        {this.demo4()}
        <br />
      </PageWrapper>
    );
  }
}

export default DetailTableDemo;
