import React, { Component } from 'react';
import { Col, Row } from 'antd';
import { InfoCircleOutlined, LoadingOutlined, ReloadOutlined } from '@ant-design/icons';
import { Dispatch } from 'redux';
import { PageWrapper } from '@/components/Layout';
import { DetailModal } from '@/components/Detail';
import styles from './DetailModalDemo.less';

interface DetailModalProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface DetailModalState {
  loading_demo2: boolean;
}

class DetailModalDemo extends Component<DetailModalProps, DetailModalState> {
  protected detailModalRef1: DetailModal | null = null;

  protected detailModalRef2: DetailModal | null = null;

  public state: DetailModalState = {
    loading_demo2: false,
  };

  // 基本使用
  protected demo1() {
    return (
      <React.Fragment>
        <DetailModal
          ref={(ref) => {
            this.detailModalRef1 = ref;
          }}
          modalWidth={800}
          columnCount={3}
          labelSpan={{ type: 'px', width: 150 }}
          borderColor="#e8e8e8"
          labels={{
            name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
            deptName: '所属部门',
            dutyType: '班次类型',
            startTime: { label: '上班时间', style: { color: 'red' } },
            endTime: '下班时间',
            onRange: '上班打卡时间范围',
            offRange: '下班打卡时间范围',
            duration: (
              <span>
                <InfoCircleOutlined /> 午休时间
              </span>
            ),
            stats: '状态',
          }}
          dataTransforms={{
            deptName: { columnCount: 2 },
            stats: [
              { value: 1, label: '成功' },
              { value: 0, label: '失败' },
            ],
          }}
          serverData={{
            initLoadData: true,
            url: '/api/query_page/detail_data',
          }}
        />
        <a
          onClick={() => {
            this.detailModalRef1?.setVisible(true);
          }}
        >
          查看
        </a>
      </React.Fragment>
    );
  }

  // 调用组件方法
  protected demo2() {
    const { loading_demo2 } = this.state;
    return (
      <DetailModal
        ref={(ref) => {
          this.detailModalRef2 = ref;
        }}
        modalWidth={800}
        modalTitle={
          <Row>
            <Col flex="auto">数据详情</Col>
            <Col flex="48px">
              {loading_demo2 ? (
                <LoadingOutlined className={styles.icon} />
              ) : (
                <ReloadOutlined
                  className={styles.reload}
                  onClick={async () => {
                    if (this.detailModalRef2) {
                      await this.detailModalRef2.refresh();
                    }
                  }}
                />
              )}
            </Col>
          </Row>
        }
        detailTableProps={{
          onLoadingChange: (loading) => this.setState({ loading_demo2: loading }),
        }}
        columnCount={3}
        labelSpan={{ type: 'px', width: 150 }}
        borderColor="#e8e8e8"
        labels={{
          name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
          deptName: '所属部门',
          dutyType: '班次类型',
          startTime: { label: '上班时间', style: { color: 'red' } },
          endTime: '下班时间',
          onRange: '上班打卡时间范围',
          offRange: '下班打卡时间范围',
          duration: (
            <span>
              <InfoCircleOutlined /> 午休时间
            </span>
          ),
          stats: '状态',
        }}
        dataTransforms={{
          deptName: { columnCount: 2 },
          stats: [
            { value: 1, label: '成功' },
            { value: 0, label: '失败' },
          ],
        }}
        serverData={{
          initLoadData: true,
          url: '/api/query_page/detail_data',
        }}
      >
        <a>查看</a>
      </DetailModal>
    );
  }

  protected demo3() {
    return (
      <DetailModal
        modalWidth={800}
        refreshOnOpen={true}
        columnCount={3}
        labelSpan={{ type: 'px', width: 150 }}
        borderColor="#e8e8e8"
        labels={{
          name: <span style={{ color: '#4A90E2' }}>班次名称</span>,
          deptName: '所属部门',
          dutyType: '班次类型',
          startTime: { label: '上班时间', style: { color: 'red' } },
          endTime: '下班时间',
          onRange: '上班打卡时间范围',
          offRange: '下班打卡时间范围',
          duration: (
            <span>
              <InfoCircleOutlined /> 午休时间
            </span>
          ),
          stats: '状态',
        }}
        dataTransforms={{
          deptName: { columnCount: 2 },
          stats: [
            { value: 1, label: '成功' },
            { value: 0, label: '失败' },
          ],
        }}
        serverData={{
          initLoadData: true,
          url: '/api/query_page/detail_data',
        }}
      >
        <a>查看</a>
      </DetailModal>
    );
  }

  render() {
    return (
      <PageWrapper pageHeaderTitle="对话框详情表格">
        <h3>基本使用</h3>
        {this.demo1()}
        <br />
        <br />
        <h3>调用组件方法</h3>
        {this.demo2()}
        <br />
        <br />
        <h3>每次显示都重新加载</h3>
        {this.demo3()}
        <br />
      </PageWrapper>
    );
  }
}

export default DetailModalDemo;
