import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { Button } from 'antd';
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { PageWrapper } from '@/components/Layout';

interface Demo_1Props {
  dispatch: Dispatch<any>;
  match: any;
}

interface Demo_1State {
  loading: boolean;
  length: number;
}

class Demo_1 extends Component<Demo_1Props, Demo_1State> {
  state: Demo_1State = {
    loading: true,
    length: 1,
  };

  render() {
    // const { match } = this.props;
    const { loading, length } = this.state;
    const divArray: React.ReactNode[] = [];
    for (let i = 0; i <= length; i++) {
      divArray.push(<div key={i}>{i}</div>);
    }
    // console.log("PageWrapper_01");
    // console.log('PageWrapper_01', match.params);
    return (
      <PageWrapper
        enablePageHeader={true}
        pageHeaderTitle="PageWrapper_01"
        pageHeaderExtra={
          <Button type="primary" icon={<CheckCircleOutlined />}>
            提交
          </Button>
        }
      >
        <Button type="primary" loading={loading}>
          PageWrapper_01
        </Button>
        <br />
        <a
          onClick={() => {
            console.log('loading -> ', !loading);
            this.setState({ loading: !loading, length: length <= 1 ? 100 : 1 });
          }}
        >
          PageWrapper_01 Demo_1
        </a>
        {divArray}
        {/* <Card bordered={false}></Card> */}

        <div>
          <span>页面8很长很长很长很长很长很长很长很长很长很长很长很长很长很长</span>
          <CloseCircleOutlined />
        </div>
        <div style={{ whiteSpace: 'nowrap' }}>
          很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很
        </div>
      </PageWrapper>
    );
  }
}

export default Demo_1;
