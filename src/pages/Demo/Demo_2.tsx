import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { Button, Card } from 'antd';

interface Demo_1Props {
  dispatch: Dispatch<any>;
}

interface Demo_1State {
  loading: boolean;
}

class Demo_1 extends Component<Demo_1Props, Demo_1State> {

  state: Demo_1State = {
    loading: true,
  }

  render() {
    const {loading} = this.state;
    console.log("Demo_2");
    return (
      <Card bordered={false}>
        <Button type="primary" loading={loading}>Demo</Button>
        <br/>
        <a
          onClick={() => {
            console.log("loading -> ", !loading);
            this.setState({loading: !loading});
          }}
        >
          loading Demo_2
        </a>
      </Card>
    );
  }
}

export default Demo_1;
