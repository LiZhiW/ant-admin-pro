import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { Button, Card } from 'antd';

interface Demo_1Props {
  dispatch: Dispatch<any>;
  match: any;
}

interface Demo_1State {
  loading: boolean;
}

class Demo_1 extends Component<Demo_1Props, Demo_1State> {

  state: Demo_1State = {
    loading: true,
  }

  render() {
    const {match} = this.props;
    const {loading} = this.state;
    const divArray: React.ReactNode[] = [];
    for (let i = 0; i <= 100; i++) {
      divArray.push(<div key={i}>{i}</div>);
    }
    console.log("Demo_1", match.params);
    return (
      <Card bordered={false}>
        <Button type="primary" loading={loading}>Demo</Button>
        <br/>
        <a
          onClick={() => {
            console.log("loading -> ", !loading);
            this.setState({loading: !loading});
          }}
        >
          loading Demo_1
        </a>
        {divArray}
      </Card>
    );
  }
}

export default Demo_1;
