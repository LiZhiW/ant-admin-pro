import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { Button, Card } from 'antd';

// import {  } from '@/components/Layout';

interface DemoPageProps {
  dispatch: Dispatch<any>;
  match: any;
}

interface DemoPageState {
  loading: boolean;
}

class DemoPage extends Component<DemoPageProps, DemoPageState> {
  state: DemoPageState = {
    loading: true,
  };

  render() {
    // const { match } = this.props;
    const { loading } = this.state;
    // eslint-disable-next-line no-console
    // console.log('match --> ', match, this.props);
    return (
      <Card bordered={false}>
        <Button type="primary" loading={loading}>
          Demo
        </Button>
        <br />
        <a onClick={() => this.setState({ loading: !loading })}>loading</a>
        <br />
        DemoPage
      </Card>
    );
  }
}

export default DemoPage;
