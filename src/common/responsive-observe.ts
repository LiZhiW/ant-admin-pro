import responsiveObserve from 'antd/es/_util/responsiveObserve';

/**
 * 响应式断点
 * <pre>
 *  xs: (max-width: 575px)
 *  sm: (min-width: 576px)
 *  md: (min-width: 768px)
 *  lg: (min-width: 992px)
 *  xl: (min-width: 1200px)
 *  xxl: (min-width: 1600px)
 * </pre>
 */
type ResponsiveBreakpoint = 'xxl' | 'xl' | 'lg' | 'md' | 'sm' | 'xs';

export { ResponsiveBreakpoint, responsiveObserve };
