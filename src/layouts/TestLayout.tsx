import React from 'react';

export interface LayoutProps extends UmiPageComponentProps {}

export interface LayoutState {}

class Layout extends React.Component<LayoutProps, LayoutState> {
  componentDidUpdate() {
    console.log('Layout -> componentDidUpdate');
  }

  render() {
    const { children } = this.props;
    return <div>{children}</div>;
  }
}

export default Layout;
