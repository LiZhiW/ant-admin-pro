import React from 'react';
import classNames from 'classnames';
import { CopyrightCircleOutlined } from '@ant-design/icons';
import logo from '@/assets/logo.svg';
import { PageHeaderModel } from '@/components/Layout';
import { AntdMenuTheme } from '@/components/Layout/layout-types';
import TopSideMenuLayout from './TopSideMenuLayout';
import styles from './TopSideLayout.less';

export interface LayoutProps extends UmiPageComponentProps {}

export interface LayoutState {}

class TopSideLayout extends React.Component<LayoutProps, LayoutState> {
  public render() {
    const { children, ...otherProps } = this.props;
    const theme: AntdMenuTheme = 'dark';
    // @ts-ignore
    const isDarkTheme = theme === 'dark';
    const header = isDarkTheme ? styles.headerDark : styles.headerLight;
    const globalHeaderTitle = isDarkTheme ? styles.globalHeaderTitleDark : styles.globalHeaderTitleLight;
    const globalHeaderMenu = isDarkTheme ? styles.globalHeaderMenuDark : styles.globalHeaderMenuLight;
    return (
      <TopSideMenuLayout
        {...otherProps}
        // 自定义样式扩展
        headerClassName={header}
        globalHeaderMenuClassName={classNames(styles.globalHeaderMenu, globalHeaderMenu)}
        globalHeaderMenuProps={{ theme }}
        sideMenuTheme="light"
        pageContentMultiTabNavClassName={styles.pageContentMultiTabNav}
        // pageContentLeftClassName={styles.pageContentLeft}
        // pageContentRightClassName={styles.pageContentRight}
        // 全局配置
        htmlTitleSuffix="Ant-Layout"
        hideGlobalFooter={false}
        headerHeight={48}
        sideMenuWidth={160}
        enableLocale={false}
        defaultOpen={true}
        menuIconScriptUrl="//at.alicdn.com/t/font_1326886_bbehrpsvyl.js"
        breadcrumbRoutesInterceptor={(layoutMenuData, routes) => (routes || []).filter((value, index) => index >= 1)}
        // 头部区域
        globalHeaderLogo={<img src={logo} alt="logo" style={{ width: 32 }} />}
        globalHeaderTitle={<div className={classNames(styles.globalHeaderTitle, globalHeaderTitle)}>Ant-Layout</div>}
        // globalHeaderOnLogoClick={() => { }}
        // 侧边栏
        sideMenuEnableSearchMenu={false}
        sideMenuBeautifyScrollbar={true}
        sideMenuAutoHideScrollbar={true}
        // 内容区域
        pageContentPageHeaderModel={PageHeaderModel.AntPageHeader}
        pageContentEnablePageHeader={false}
        pageContentShowHomeButton={true}
        pageContentShowMoreButton={true}
        pageContentBeautifyScrollbar={true}
        pageContentAutoHideScrollbar={false}
        // 页脚区域
        globalFooterLinks={[]}
        globalFooterCopyright={
          <>
            Copyright <CopyrightCircleOutlined /> 2019 武汉XX科技有限公司 鄂ICP备19029XXX号
          </>
        }
      >
        {children}
      </TopSideMenuLayout>
    );
  }
}

export default TopSideLayout;
