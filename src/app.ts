// umi 运行时配置 https://umijs.org/zh/guide/runtime-config.html

/**
 * 初始化全局状态
 */
const getInitialState = async () => {
  return {};
};

/**
 * 运行时修改路由
 */
// const patchRoutes = (routes) => {
//   console.log("routes-->", routes[1].component);
//   const route = routes[1];
//   route.component = TopAndSideLayout;
// }

/**
 * 改写把整个应用 render 到 dom 树里的方法
 */
// const render = (oldRender) => {
//   console.log("oldRender");
//   oldRender();
// }

// dva配置 https://github.com/umijs/umi/issues/1051
// const dva = {
//   config: {
//     initialState: {},
//     onError(err: any) {
//       err.preventDefault();
//     },
//   },
// };

export {
  getInitialState,
  // dva,
  // render,
  // patchRoutes,
};
