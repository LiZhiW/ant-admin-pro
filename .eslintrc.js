const eslint = require.resolve('@umijs/fabric/dist/eslint');

module.exports = {
  extends: [eslint],

  globals: {
    API_GLOBAL_PREFIX: true,
    ENABLE_CDN: true,
  },

  rules: {
    'arrow-parens': 0,
    'arrow-body-style': 0,
    'linebreak-style': 0,
    'class-methods-use-this': 0,
    'no-continue': 0,
    'no-plusplus': 0,
    'no-nested-ternary': 0,
    'prefer-destructuring': 0,
    camelcase: 0,
    'no-shadow': 0,
    'no-empty': 0,
    'no-use-before-define': 0,
    'default-case': 0,
    'no-case-declarations': 0,
    'no-param-reassign': 0,
    'no-else-return': 0,
    'no-underscore-dangle': 0,
    'no-undef-init': 0,

    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/naming-convention': 0,
    '@typescript-eslint/no-unused-vars': 0,

    'react/jsx-filename-extension': [1, { extensions: ['.tsx'] }],
    'react/jsx-wrap-multilines': 0,
    'react/prop-types': 0,
    'react/forbid-prop-types': 0,
    'react/jsx-one-expression-per-line': 0,
    'react/jsx-boolean-value': 0,
    'react/sort-comp': 0,
    'react/prefer-stateless-function': 0,
    'react/no-array-index-key': 0,
    'react/jsx-curly-brace-presence': 0,

    'import/no-unresolved': [2, { ignore: ['^@/', '^umi/'] }],
    'import/no-extraneous-dependencies': 0,
    'import/no-cycle': 0,
    'import/prefer-default-export': 0,

    'jsx-a11y/no-noninteractive-element-interactions': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/anchor-is-valid': 0,
  },
};
