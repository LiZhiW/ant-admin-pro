declare module 'antd-dayjs-webpack-plugin';
declare module 'slash2';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.svg';
declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.gif';
declare module '*.bmp';
declare module '*.tiff';
declare module 'omit.js';

// interface Window {
//   g_app: {
//     _store: any;
//     [propName: string]: any;
//   };
// }

/** api全局url前缀 */
declare const API_GLOBAL_PREFIX: string;

/** 是否是生产环境 */
declare const IS_PROD_ENV: boolean;
