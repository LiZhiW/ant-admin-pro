/** 路径变量 */
type PathVariable = { [path: string]: string | number | boolean };
/** url querystring 部分 */
type QueryString = { [param: string]: string };
/** 路由权限配置 */
type RouterAuthorityConfig = string | string[]; // TODO 支持函数 | ((ctx) => boolean);

/** 路由的子面包屑配置 */
interface BreadcrumbChildren {
  /** 跳转路径 */
  redirectPath: string;
  /** 面包屑文本 */
  breadcrumbName: string;
  /** 面包屑文本-国际化文本ID (默认自动生成) */
  breadcrumbNameLocale?: string;
  /** 菜单路径中有变量时，配置的路劲变量对象 */
  pathVariable?: PathVariable;
  /** 设置菜单路径的queryString部分 */
  querystring?: QueryString;
  // /** 扩展属性 */
  // [property: string]: any;
}

/** 运行时路由的子面包屑数据 */
interface RuntimeBreadcrumbChildren extends BreadcrumbChildren {
  /** 面包屑文本-国际化文本ID (默认自动生成) */
  breadcrumbNameLocale: string;
  /** 菜单路径中有变量时，配置的路劲变量对象 */
  pathVariable: PathVariable;
  /** 设置菜单路径的queryString部分 */
  querystring: QueryString;
  // /** 扩展属性 */
  // [property: string]: any;
}

/** 路由配置(扩展umi原生的IRoute) */
interface RouterConfig {
  // ------------------------------------------------------------------------------------------------------------------------------------------------------------ IRoute
  /** 路由路径，可以被 path-to-regexp@^1.7.0 理解的路径通配符 */
  path: string;
  /** 表示是否严格匹配，即 location 是否和 path 完全对应上 */
  exact?: boolean;
  /** 配置 location 和 path 匹配后用于渲染的 React 组件路径。可以是绝对路径，也可以是相对路径，如果是相对路径，会从 src/pages 开始找起 */
  component?: string;
  /** 配置子路由，通常在需要为多个路径增加 layout 组件时使用 */
  routes?: RouterConfig[];
  /** 配置路由的高阶组件封装 */
  wrappers?: string[];
  /**
   * Html页面Title(默认支持国际化 <br/>
   * @deprecated 不建议使用，相关配置项：name、nameLocale、pageTitle、pageTitleLocale)
   */
  title?: string;
  /** 配置路由跳转(前端路由跳转) */
  redirect?: string;
  // ------------------------------------------------------------------------------------------------------------------------------------------------------------ 扩展配置
  /** 菜单图标(“icon-”前缀表示使用“阿里巴巴矢量图标库”) */
  icon?: string;
  /** 菜单路径中有变量时，配置的路径变量对象 */
  pathVariable?: PathVariable;
  /** 设置菜单路径的queryString部分 */
  querystring?: QueryString;
  /** 菜单名称 */
  name: string;
  /** 菜单名称-国际化文本ID (默认自动生成) */
  nameLocale?: string;
  /** Html页面Title(不配置就默认取“name”) */
  pageTitle?: string;
  /** Html页面Title-国际化文本ID (默认自动生成) */
  pageTitleLocale?: string;
  /** 面包屑文本(不配置就默认取“name”) */
  breadcrumbName?: string;
  /** 面包屑文本-国际化文本ID (默认自动生成) */
  breadcrumbNameLocale?: string;
  /** 当前面包屑的子面包屑配置 */
  breadcrumbChildren?: BreadcrumbChildren[];
  /** 是否隐藏面包屑导航 */
  hideBreadcrumb?: boolean;
  /** 菜单分组名称(配置才会分组，不配置就不分组) */
  groupName?: string;
  /** 菜单分组名称-国际化文本ID (默认自动生成) */
  groupNameLocale?: string;
  /** 默认展开子菜单 */
  defaultOpen?: boolean;
  /** 隐藏当前菜单和子菜单 */
  hideMenu?: boolean;
  /** 隐藏子菜单 */
  hideChildrenMenu?: boolean;
  /** 是否启用多语言 */
  enableLocale?: boolean;
  /** 菜单权限控制(权限字符串) */
  authority?: RouterAuthorityConfig;
  // /** 扩展属性 */
  // [property: string]: any;
}

/** 运行时路由 */
interface RuntimeRouter extends RouterConfig {
  // Omit<RouterConfig, 'component' | 'routes'> {
  /** 对应的React组件 */
  component: React.Component;
  /** 配置子路由，通常在需要为多个路径增加 layout 组件时使用 */
  routes?: RuntimeRouter[];
  /** 菜单路径中有变量时，配置的路径变量对象 */
  pathVariable: PathVariable;
  /** 设置菜单路径的queryString部分 */
  querystring: QueryString;
  /** 菜单名称-国际化文本ID (默认自动生成) */
  nameLocale: string;
  /** Html页面Title(不配置就默认取“name”) */
  pageTitle: string;
  /** Html页面Title-国际化文本ID (默认自动生成) */
  pageTitleLocale: string;
  /** 面包屑文本(不配置就默认取“name”) */
  breadcrumbName: string;
  /** 面包屑文本-国际化文本ID (默认自动生成) */
  breadcrumbNameLocale: string;
  /** 当前面包屑的子面包屑配置 */
  breadcrumbChildren: RuntimeBreadcrumbChildren[];
  /** 是否隐藏面包屑导航 */
  hideBreadcrumb: boolean;
  /** 隐藏当前菜单和子菜单 */
  hideMenu: boolean;
  /** 隐藏子菜单 */
  hideChildrenMenu: boolean;
  // /** 扩展属性 */
  // [property: string]: any;
}

/** 运行时菜单项 */
interface RuntimeMenuItem {
  /** 路由配置 */
  routerConfig: RuntimeRouter;
  /** 当前菜单唯一Key */
  menuKey: string;
  /** 当前菜单的所有上级菜单的Key数组 */
  parentKeys: string[];
  /** 子菜单项 */
  children: RuntimeMenuItem[];
  /** 根据配置规则，当前菜单是否隐藏 */
  isHide: boolean;
  // /** 扩展属性 */
  // [property: string]: any;
}

/** 路由页签项(多页签项) */
interface MultiTabItem {
  /** 路由菜单项 */
  menuItem: RuntimeMenuItem;
  /** 路由页签项唯一Key */
  multiTabKey: string;
  /** 当前url路径 */
  currentPath: string;
  /** 当前页面location状态 */
  location: UmiLocation;
  /** 是否是首页 */
  isHomePage: boolean;
  /** 当前页签是否是选中状态 */
  active: boolean;
  /** 最后一次活动时间(时间戳) */
  lastActiveTime: number;
  /** 是否显示关闭按钮 */
  showClose: boolean;
}

/** 全局Layout菜单数据 */
interface LayoutMenuData {
  /** 根菜单 */
  rootMenu: RuntimeMenuItem;
  /** 根菜单(过滤隐藏的菜单) */
  showRootMenu?: RuntimeMenuItem;
  /**
   * 拍平的菜单数据
   * <pre>
   *   Map<RuntimeMenuItem.routerConfig.path, RuntimeMenuItem>
   * </pre>
   */
  flattenMenuMap: Map<String, RuntimeMenuItem>;
  /**
   * 提供外部用户使用
   * <pre>
   *   { path: MenuDataItem }
   * </pre>
   */
  flattenMenu: { [path: string]: RuntimeMenuItem };
  /** 当前访问Url地址 */
  currentPath: string;
  /** 当前访问页面对应的菜单 */
  currentMenu?: RuntimeMenuItem;
  /** 当前访问页面对应的显示菜单(显示逻辑对应关系) */
  showCurrentMenu?: RuntimeMenuItem;
}

/**  */
/**  */
/**  */

/**  */

/** 路由菜单设置 */
interface RouterMenuSettings {
  /** 是否启用多语言 */
  enableLocale?: boolean;
  /** 默认展开子菜单 */
  defaultOpen?: boolean;
}

/** 全局Layout配置 */
interface LayoutSettings {
  /** 菜单配置 */
  menu: RouterMenuSettings;
}
