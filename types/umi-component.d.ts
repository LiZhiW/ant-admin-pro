/** umi 传进来的 location 信息 */
interface UmiLocation {
  /** url的path部分 */
  pathname: string;
  /** url的querystring部分(以"?"前缀开始) */
  search: string;
  /** url的hash部分(#号后面部分) */
  hash: string;
  /** url的querystring解析结果(一个对象) */
  query: { [name: string]: any };
  /** router.push 传入的 state */
  state?: { [name: string]: any };
}

/** 页面组件 */
interface UmiPageComponentProps {
  /** 路由信息 */
  route: RuntimeRouter;
  /** React组件的location信息 */
  location: UmiLocation;
  /** 路由匹配信息 */
  match: {
    /** 匹配参数 */
    params: { [param: string]: string };
    /** 是否是严格匹配 */
    isExact: boolean;
    /** URL path 字符串 */
    path: string;
    /** URL 字符串 */
    url: string;
  };
  /** 路由数组 */
  routes: Array<RuntimeRouter>;
  /** */
  children: React.Component;
  /** */
  history: object;
  /** */
  staticContext?: object;
}

// history:
//   action: "POP"
//   block: ƒ block(prompt)
//   createHref: ƒ createHref(location)
//   go: ƒ go(n)
//   goBack: ƒ goBack()
//   goForward: ƒ goForward()
//   length: 8
//   listen: ƒ listen(listener)
//   location: {pathname: "/demo/page2", search: "?aa=asa", hash: "#aas", query: {…}, state: undefined}
//   push: ƒ push(path, state)
//   replace: ƒ replace(path, state)
