declare module 'simplebar-react' {
  // https://github.com/Grsmto/simplebar/blob/master/packages/simplebar/README.md#options
  interface Props extends React.HTMLAttributes<HTMLElement> {
    scrollableNodeProps?: object;
    options?: object;
    /** 滚动条是否自动影藏(Default value is true) */
    autoHide?: boolean;
    /** 可以使用force visible选项强制轨迹可见（与overflow:scroll的行为相同） (default to `false`) */
    forceVisible?: boolean | 'x' | 'y';
    /** 您可以通过传递“方向”选项来激活RTL支持(default to `ltr`) */
    direction?: 'ltr' | 'rtl';
    /** 定义滚动条隐藏之前的延迟。如果autoHide为false，则不起作用(Default value is 1000) */
    timeout?: number;
    /** 控制单击轨迹的行为(Default to true) */
    clickOnTrack?: boolean;
    /** 以像素为单位定义最小滚动条大小(Default value is 10) */
    scrollbarMinSize?: number;
    /** 以像素为单位定义最大滚动条大小(Default for scrollbarMaxSize is 0 (no max size)) */
    scrollbarMaxSize?: number;
    /** 可以更改SimpleBar使用的默认类名。要使用自己的样式，请参阅simplebar.css以了解如何设置css */
    classNames?: {
      content?: string;
      scrollContent?: string;
      scrollbar?: string;
      track?: string;
    };
  }

  export default class SimpleBar extends React.Component<Props> {}
}
